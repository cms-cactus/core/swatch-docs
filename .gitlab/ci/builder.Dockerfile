FROM cern/cc7-base:20200715-1.x86_64
LABEL maintainer="Cactus Role Account <cactus@cern.ch>"

RUN yum install -y python-pip make && \
    yum clean all
RUN pip install --upgrade pip && \
    pip install -U sphinx sphinx_rtd_theme