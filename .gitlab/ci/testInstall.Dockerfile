# replicate a P5 installation by starting from a fresh CC7 installation
# and installing everything from RPM repos that are supposed to be
# replicated to P5

FROM cern/cc7-base:20200715-1.x86_64
LABEL maintainer="Cactus Role Account <cactus@cern.ch>"

ARG BUILD_UTILS_VERSION=0.1.0

WORKDIR /etc/yum.repos.d

COPY usersGuide/yum-repo-files/*.repo ./

RUN curl -OL https://gitlab.cern.ch/cms-cactus/core/cactus-buildenv/-/raw/master/cactus-build-utils.noarch.repo

RUN \
  yum install -y \
    log4cplus \
    createrepo \
    bzip2-devel \
    zlib-devel \
    ncurses-devel \
    python-devel \
    curl curl-devel \
    graphviz graphviz-devel \
    wxPython \
    e2fsprogs-devel \
    libuuid-devel \
    qt qt-devel \
    libusb libusb-devel \
    gd-devel \
    xsd valgrind \
    vim \
    jsoncpp-devel \
    readline-devel \
    centos-release-scl devtoolset-8-gcc devtoolset-8-gcc-c++ \
    cactuscore-build-utils-$BUILD_UTILS_VERSION \
	 && \
  yum groupinstall -y "Development Tools" \
    uhal amc13 mp7 \
    cmsos_core \
    cmsos_core_debuginfo \
    cmsos_worksuite \
    cmsos_worksuite_debuginfo \
    triggersupervisor \
    swatch swatchcell \
    # we're not intalling JAVA just yet
    --exclude cmsos-worksuite-dipbridge \
    --exclude cmsos-worksuite-amc13controller

RUN yum list installed