.. SWATCH documentation master file, created by
   sphinx-quickstart on Sun Feb 14 23:12:02 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SWATCH's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   usersGuide/index
   commonPlugins/index
   releaseNotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

