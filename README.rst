SWATCH documentation
====================

The SWATCH documentation is based on sphinx, a tool which can convert reStructuredText-format (``.rst``) source files into any one of several output formats - HTML, LaTeX (and thereby PDF), ePub, man pages, etc.

In order to find out more info about sphinx, including installation instructions, see https://www.sphinx-doc.org

Building
--------

This documentation was set up using ``sphinx-quickstart``, which produced a ``Makefile`` that runs the sphinx build commands. 

In order to build the HTML documentation, you just need to run::

  make html

The output can then be found under ``_build``; e.g. on a mac, to view the output, just run ``open _build/html/index.html``

If you have changed any hyperlinks, it's also useful to run ``make linkcheck`` which will report if any hyperlinks to external pages are broken.


Deploying
---------

The documentation is deployed under the ``doc`` subdirectory of the relevant SWATCH release directory of the CACTUS ``www`` area; e.g. the documentation for the X.Y release series is stored under ``~cactus/www/release/swatch/X.Y/doc/html``, and can be deployed simply using the following ``scp`` command::

  ## If the directory already exists, first run:  ssh cactus@lxplus "rm -rf ~/www/release/swatch/X.Y/doc/html" first
  scp -r _build/html cactus@lxplus:~/www/release/swatch/X.Y/doc/html

**N.B.** If deploying the documentation of for a new release, you should also update the ``latest_doc`` link::

  ln -s /afs/cern.ch/user/c/cactus/www/release/swatch/X.Y/doc latest_doc
