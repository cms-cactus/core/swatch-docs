
Maskable objects
================

Due to hardware failure in other parts of the trigger, or in the sub-detectors' electronics, during standard running various data-paths within the trigger must be temporarily masked (i.e. reset to zeros, or some other constant value, at the interface to downstream algorithms); for example:

* **Input ports:** Must be masked if the optical transmitter at the start of that link is not configured, or in error.
* Some **sub-set of data transmitted over an optical link** is physically incorrect (e.g. hot ECAL towers)
* The **AMC13's backplane DAQ ports** corresponding to disabled processors must not be configured while those processors is disabled.

Such masks form part of the trigger's "run settings" parameters. They:

* affect the dataflow through the trigger, and
* often need to be transmitted to the offline database via O2O.

Therefore, we need to have a standardised approach to these masks; i.e. a standardised C++ interface, as well as a standard specification of masks in XML configuration files and the online database

In C++, the maskable components are represented by the ``swatch::core::MaskableObject`` interface class, which:

* Provides access to a **boolean mask flag** through its public API: ``bool isMasked() const;``
* Provides a public method to set the mask flag: ``void setMasked(bool aMask=true);``
* Is inherited by:

  * ``swatch::processor::InputPort``
  * ``swatch::dtm::AMCPort``

i.e. both input ports and the AMC13's backplane DAQ ports are maskable. Other components within your processors can be made maskable by simply representing those components in the SWATCH object tree with an instance of ``swatch::core::MaskableObject``, or any other class that inherits from ``MaskableObject``.

Components within a processor/AMC13 can be specified as masked in XML configuration files using the ``mask`` tag. This tag should be used as a child of the ``context`` tag. It has one attribute, ``id``, the ID path of the object that should be masked (relative to the processor's / AMC13's ID path); e.g. ``inputPorts.Rx00`` for input port ``Rx00`` within a processor. **N.B.** The same context look-up order is used for masked objects as for command parameters (e.g. ``systemID.processorID`` , ``systemID.processorRole`` , ``systemID.processorID`` for processors).

For example, the following XML file snippet specifies that input port "Rx00" is masked on all of the system's processors, and additionally input ports "Rx40", "Rx41", "Rx42" and "Rx45" are also masked on processor with ID "procC":

.. code-block:: xml

  <run-settings id="dummySys">
     <!-- INPUT PORT 0: masked for all processors -->
     <context id="processors">
        <mask id="inputPorts.Rx00" />
     </context>

     <!-- INPUT PORTS 40-42 & 45: masked only for processor with ID "procC" -->
     <context id="procC">
           <mask id="inputPorts.Rx40" />
           <mask id="inputPorts.Rx41" />
           <mask id="inputPorts.Rx42" />
           <mask id="inputPorts.Rx45" />
     </context>
  </run-settings>

The boolean **mask flags are automatically updated when FSMs are engaged and reset** . Specifically, when a processor's/AMC13's state machine is engaged/reset:

#. All maskable components are reset to being unmasked
#. The masks specified in the gatekeeper (i.e. XML file / online database) are then applied

**Note for development and debugging:** The "maskable objects" panel displays a list of all of the maskable objects that are currently masked.
