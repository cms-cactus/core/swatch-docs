
Commands
========

Each subsystem can define their own set of commands for their specializations of the processor, DaqTTCManager and system classes.

Each command is implemented by creating a class that inherits from ``swatch::action::Command`` , and defining the following method:

.. code-block:: c++

  swatch::action::Comand::State code(const swatch::core::XParameterSet& parameters);

This method contains the meat of the command class, in which the action is implemented; from within this method, the subsystem-specific command implementation can interact with framework and resources as follows:

* **Accessing the resource**

  * The associated resource class (processor, DaqTTCManager or system) can be accessed from within this method using template method ``T& getActionable<T>()``

* **Parameters**

  * If parameters are required for this command, then they can be registered in the class's constructor, and the parameter values can be extracted from the method's argument; the parameters must be derived from ``xdata::Serializable``.
  * In the SWATCH TS example cell, the values of these parameters can be set through the command GUI before running a command; eventually, these parameter values will be extracted from the DB (or a configuration file) by the SWATCH cell framework.

* **Return value** The return value is used to communicate whether:

  * there was an error (``return State::kError;``); or
  * there was no error, but something strange constituting a warning did happen (``return State::kWarning;``); or
  * everything went fine (``State::kDone;``)
  * **N.B.** ``State::kDone/kWarning/kError`` are the only valid return values.

* *Updating progress for the outside world* The ``swatch::action::Command`` `API <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d7/d76/classswatch_1_1action_1_1_command.html>`__ exposes the progress of each command via a status string, and a float representing the fractional progress (from 0 at the beginning of a command, to 1 at its completion); these values should be updated in all commands lasting more than roughly 5 seconds, in order to allow users running the command to see their progress (e.g. in the TS cell's web interface). The status string and fractional progress value can be set from within the ``code`` method by calling the following methods of ``swatch::action::Command``

  * ``setProgress(float)``
  * ``setProgress(float, const std::string&)``
  * ``setStatusMsg(const std::string&)``

* *Result* Each SWATCH command also has a result, whose type must inherit from ``xdata::Serializable``.

  * The type and default value of the command's result is defined by calling the templated ``swatch::action::Command`` constructor, ``Command(const std::string& id, swatch::action::ActionableObject& aActionable, const ResultType & )`` ; the default value for the result is given as the 2nd argument in this constructor, which will be called from within the constructor of the subsystem-specific command class.
  * The value of the result can be set from within the ``code`` method by calling the ``setResult(xdata::Serialisable&);`` method - e.g. ``setResult( xdata::Integer(42) )``.

Once a class inheriting from ``swatch::action::Command`` has been implemented, it needs to be registered in the constructor of the resource that it's acting upon (typically the processor, or DaqTTCManager), by calling the ``registerFunctionoid<T>(const std::string& id)`` method. E.g. if the ``MyResetCommand`` class acts on a processor represented by the ``MyProcessor`` class, then it should be registered by adding the following line to ``MyProcessor`` 's constructor :

.. code-block:: c++

  registerFunctionoid<MyResetCommand>("myCoolResetCommand");


Implementation example
----------------------

A command which just takes several sleeps, based on parameters ``n`` and ``millisecPerSleep``, with ``DummyActionableObject`` as the associated resource, can be implemented as follows:

* Header file:

  .. code-block:: c++


    #include "swatch/action/Command.hpp"


    class DummySleepCommand: public swatch::action::Command {
    public:
      DummySleepCommand( const std::string& aId, swatch::action::ActionableObject& aActionable );

      virtual ~DummySleepCommand();

      virtual swatch::action::Command::State code(const XParameterSet& params);
    };


* cpp file:

  .. code-block:: c++


    #include "path/to/DummySleepCommand.hpp"


    #include <sstream>

    #include "boost/chrono.hpp"

    #include "xdata/UnsignedInteger.h"

    #include "path/to/DummyActionableObject.hpp"


    DummySleepCommand::DummySleepCommand(const std::string& aId, swatch::action::ActionableObject& aActionable) :
      swatch::action::Command(aId, aActionable, xdata::Integer(-33))
    {
      registerParameter("n", xdata::UnsignedInteger(50));
      registerParameter("millisecPerSleep", xdata::UnsignedInteger(100));
    }


    DummySleepCommand::~DummySleepCommand()
    {
    }


    swatch::action::Command::State DummySleepCommand::code(const XParameterSet& params)
    {
      DummyActionableObject& res = getActionable<DummyActionableObject>();

      setStatusMsg("Dummy sleep command just started");

      unsigned int n(params.get<xdata::UnsignedInteger>("n").value_);
      unsigned int millisecPerSleep(params.get<xdata::UnsignedInteger>("millisecPerSleep").value_);

      for (unsigned int i = 0; i < n; i++) {
        boost::this_thread::sleep_for(boost::chrono::milliseconds(millisecPerSleep));

        std::ostringstream oss;
        oss << "Dummy sleep command for object '" << res.getPath() << "' progressed. " << i << " of " << n << " sleeps done; " << (n - i) * millisecPerSleep << " milli sec remain";
        setProgress( float(i) / n, oss.str());
      }

      setStatusMsg("Dummy sleep command for object '" + res.getPath() + "' completed");
      return State::kDone;
    }


This command could then be registered with ID string ``"sleep"`` in the ``DummyActionableObject`` constructor as follows:

.. code-block:: c++

  DummyActionableObject::DummyActionableObject( ... )
  {
    register<DummySleepCommand>("sleep");
  }


.. _usersGuide-commands-parameterValidation:

Input parameter validation
--------------------------

.. note:: New in SWATCH v1.0

When you implement a command, certain parameter values - or combinations of the values of multiple parameters - may not make sense. For example:

* Many of the simple ``xdata`` types - such as ``xdata::Integer``, ``xdata::Float``, and ``xdata::Boolean`` - provide a ``NaN`` value. For certain commands ``NaN`` may not easily map onto a default value, or different behaviour.
* For two integer parameters - ``minValue`` and ``maxValue`` - indicating a range-type setting in the firmware, you would expect ``minValue <= maxValue``

Such rules on the possible values of parameters can be registered through the ``Command`` class' API. The SWATCH framework automatically checks that all rules and constraints on parameter values are satisfied before any transition/command is executed.

If parameter value rules are simply implemented in the command class' ``code`` method, then invalid parameter values would only be caught when running the actual command - e.g. midway through the execution ``configure`` or ``align`` transition. However, if parameter rules and constraints are declared separate to the ``code`` method as presented below, invalid parameter values will be caught before the relevant transition starts, allowing problems with a subsystem's configuration parameters to be found and fixed more promptly.

You can register two types of parameter value checks through the SWATCH API:

#. *Rules* that apply to the value of an individual parameter, independent of the values of other parameters
#. *Constraints* on the values of multiple parameters (e.g. the ``minValue <= maxValue`` example given above)

.. note:: You must not contact your hardware during the evaluation of a rule / constraint. 


Single-parameter rules
^^^^^^^^^^^^^^^^^^^^^^

A rule for the value of an individual parameters is set through the third argument in the ``registerParameter`` method.

The rule that is applied to the parameter must be implemented in a class that inherits (directly/indirectly) from the ``swatch::core::XRule<ParameterType>`` class; the derived class must implement two methods:

* ``void describe(std::ostream& aStream) const`` should stream a short plaintext description of the rule - e.g. ``isFinite(x)``
* ``swatch::core::XMatch verify(const ParameterType& aValue) const`` which implements the rule, and returns a instance of the ``XMatch`` struct that contains two member variables:

  * ``bool ok`` - equal to ``true`` if the parameter's value is "good", ``false`` if the value is invalid (i.e. fails this rule)
  * ``std::string details`` - a brief description of the reason that the parameter 'failed' the rule; should be empty if the parameter 'passed' the rule, or if the reason for the parameter violating the rule is obvious (e.g. in case of the rule ``x < upperLimit`` on an integer/float parameter)

Several common generic rules are already declared in the SWATCH framework, within the ``swatch::core::rules`` namespace (see `this doxygen page <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/db/d3e/namespaceswatch_1_1core_1_1rules.html>`__ for the full list); in particular:

* ``FiniteNumber<ParameterType>`` requires that ``xdata`` boolean, (unsigned) integer, float and double classes have a finite value (i.e. do not equal ``NaN`` or ``+/-Inf``)
* ``FiniteVector<VectorType>`` requires that all elements in vectors of simple types pass the ``FiniteNumber`` rule 
* ``NoEmptyCells`` requires that none of the cells in a ``xdata::Table`` are empty
* ``None<ParameterType>`` represents no rule - i.e. all parameter values pass this rule

When a parameter is defined without provding the third 'rule' argument to the ``registerParameter`` method, a default rule - that is expected to cover most normal use cases - is used. The default rules for each parameter type are:

===========================================  =====================================
**Parameter type**                           **Default rule**
===========================================  =====================================
``xdata::UnsignedInteger``                   ``FiniteNumber``
``xdata::Integer``                           ``FiniteNumber``
``xdata::Boolean``                           ``FiniteNumber``
``xdata::Float``                             ``FiniteNumber``
``xdata::String``                            ``None``
``xdata::Vector<xdata::UnsignedInteger>``    ``FiniteVector``
``xdata::Vector<xdata::Integer>``            ``FiniteVector``
``xdata::Vector<xdata::Boolean>``            ``FiniteVector``
``xdata::Vector<xdata::Float>``              ``FiniteVector``
``xdata::Vector<xdata::String>``             
``xdata::Table``                             ``NoEmptyCells``
===========================================  =====================================


Multi-parameter constraints
^^^^^^^^^^^^^^^^^^^^^^^^^^^
* 