Defining parameter values
=========================

When running commands as part of a command sequence or the transition of an FSM, the values for each of the commands' parameters are taken from an XML file / from the database, via the data-source-agnostic interface of the ``GateKeeper`` class.

Contexts and namespaces
-----------------------

The parameter extracted from the XML file / database are arranged in contexts; each context contains a map of ``xdata::Serialisable`` objects identified by strings. Common parameters can be specified once in common contexts; different values can be used for individual processors / DaqTTCManagers through the contexts specific to that individual object.

The rules for which parameter value is used for each command are:

* Look first in the context specific to this processor / DaqTTCManager, then look in the context of common parameter values that are shared by multiple processors / DaqTTCManagers.
* Within each context, look for parameter IDs:

  * ``namespaceID.commandID.parameterName``
  * ``commandID.parameterName``
  * ``parameterName``

* Use the first parameter value found in this search order
* If no parameter is found in this search, the command sequence / transition will not run.

For processors, the following contexts are used: first ``systemID.processorID``; then ``systemID.processorRole``, then ``systemID.processors`` . E.g. for a processor with ID string ``processorA``, and role ``someRole`` in system ``mySystem``, the following order of parameter contexts is used:

* ``mySystem.processorA``
* ``mySystem.someRole``
* ``mySystem.processors``

Similarly, for DaqTTCManager, the following contexts are used: first ``systemID.daqttcMgrID``; then ``systemID.daqttcMgrRole``, then ``systemID.daqttcs``. E.g. for a DaqTTCManager with ID string ``amc13a``, and role ``someAMC13Role`` in system ``mySystem``, the following order of parameter contexts is used:

* ``mySystem.amc13a``
* ``mySystem.someAMC13Role``
* ``mySystem.daqttcs``

Hence, the ``ROLE`` field in the system description file allows you to apply different default parameter values to different sets of processors/DaqTTCManagers from the same system, without having to duplicate entries in the XML file / database contexts.

The ``systemID.processors`` and ``systemID.daqttcs`` contexts allow you to specify default parameter values for all processors / DaqTTCManagers in a system.

When a command is added to a sequence (or when a command / command sequence is added to a transition), you can also define an alias which will be used instead of the default namespace (the sequence/transtion's ID string) when looking for the parameter values for that command. In particular, this is useful if you include the same command twice in the same sequence/transition, but want to use different parameter values the second time it's run, compared with the first.


XML file syntax
---------------

*New in v0.4* The "configuration data" is specified by a single top-level XML config file, which references one or more config module files. The advantages of splitting the configuration data over multiple files are that:

* It allows you to separate parameters into different files based on their logical purpose; e.g. put technical parameters (alignment points, clock configuration, etc) in one file, physics parameters (energy thresholds, calibration look-up tables, etc) in another file
* It allows certain groups of parameters to be re-used between different configuration keys without having to copy-paste long sections of files.
* It more closely matches the intended layout of data in the 2016 online database

The *top-level file* must have one root ``db`` start-/end-tag, which should contain the following structure:

* Each file can containg multiple sets of "run keys" (the XML tag ``key``); only the parameter values from one "run key" are loaded at any given time
* Within each key, you can include one or more sub-config module files. These sub-config files are specified using the XML tag ``load`` , with the ``module`` attribute defining the relative path of the sub-config file with respect to the top-level file.

An example top-level config file is:

.. code-block:: xml

  <db>
    <key id="RunKey1">
      <load module="technicalParameters.xml"/>
      <load module="lookUpTables.xml"/>
    </key>
  </db>


The **config module files** must have one root ``module`` start-/end-tag, which should contain the following structure:

* The contexts are specified, using the XML ``context`` tag, as children of the ``module`` tag. The ``context`` tag's ``id`` attribute defines the context's ID string; as explained in the previous section, the IDs of the contexts used for processors and DaqTTCManagers are of the following formats:

  * *Processors:* ``systemID.processorID``, ``systemID.processorRole``, ``systemID.processors``
  * *DaqTTCManager:* ``systemID.daqttcManagerID``, ``systemID.daqttcManagerRole``, and ``systemID.daqttcs``

* Within each context, you can specify one or more parameter values using the XML tag ``param``, with the string representation of the value specified between the start and end tag, the parameter ID specified by the ``id`` attribute, and the C++ data type specified by the ``type`` attribute. The following C++ types are currently available:

  ===========================================  =====================================
  **C++ type**                                 **Value of param tag's ID attribute**
  ===========================================  =====================================
  ``xdata::UnsignedInteger``                   ``uint``
  ``xdata::UnsignedInteger64``                 ``uint64``
  ``xdata::Integer``                           ``int``
  ``xdata::Boolean``                           ``bool``
  ``xdata::Float``                             ``float``
  ``xdata::String``                            ``string``
  ``xdata::Vector<xdata::UnsignedInteger>``    ``vector:uint``
  ``xdata::Vector<xdata::Integer>``            ``vector:int``
  ``xdata::Vector<xdata::Boolean>``            ``vector:bool``
  ``xdata::Vector<xdata::Float>``              ``vector:float``
  ``xdata::Vector<xdata::String>``             ``vector:string``
  ``xdata::Table``                             ``table``
  ===========================================  =====================================

For the vector and table types, the default delimiter is a comma - e.g. a vector of three integers would be written as: ``42, 56, 123``

**Note:** This may not be an exhaustive list of all types that will be needed for configuring the trigger; if you need more types to be added to this list, please get in touch with us (Tom Williams, Alessandro Thea) and we'll be happy to do so.


.. _usersGuide-configParameters-tables:

Table syntax
^^^^^^^^^^^^

The definitions of the columns of a ``xdata::Table``, and the data stored in each row, are specified by adding the following  XML tags as children of the ``param`` tag:

* ``<columns>``: No attributes; its contents should be a delimited list of titles for the columns

  * E.g. Using the default delimiter: ``<columns>title of 1st column , title of 2nd column <columns>``

* ``<types>``: No attributes; its contents should be a delimited list of the type of the data for each column; valid types are:

  * E.g. Using the default delimiter: ``<types> string, uint <columns>``

* ``<rows>``: No attributes; its contents should be a series of row tags. Each `row` tag specifies the data for one row in the table.

  * E.g. Using the default delimiter:

    .. code-block:: xml

      <rows>
        <row> a value for 1st row , 42 </row>
        <row> other value for 2nd row , 76 </row>
      </rows>


*N.B.* the default column delimiter is a comma (i.e. ``,``); this can be changed through the ``delimiter`` attribute of the ``param`` tag.

*N.B.* Any whitespace directly before/after the delimiters is removed.

For example, the following XML speicifies a table containing 3 columns:

#. ``algo_name``, type ``string``
#. ``algo_index``, type ``xdata::UnsignedInteger``
#. ``some_flag``, type  ``xdata::Boolean``


.. code-block:: xml

  <param id="myTable" type="table">
    <columns>algo_name, algo_index, some_flag</columns>
    <types>string, uint, bool</types>
    <rows>
      <row>someAlgo, 0, true</row>
      <row>anotherAlgo, 42, false</row>
    </rows>
  </param>


The following XML contains the same table, but with a non-default delimiter (rather than the default delimiter):


.. code-block:: xml

  <param id="myTable" type="table" delimiter="|">
    <columns>algo_name| algo_index| some_flag</columns>
    <types>string| uint| bool</types>
    <rows>
      <row>someAlgo| 0| true</row>
      <row>anotherAlgo| 42| false</row>
    </rows>
  </param>



Example configuration modules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some example config module files are:

* ``technicalParameters.xml``:

  .. code-block:: xml

    <infra id="dummySys">
       <!-- Common parameters for all processors -->
       <context id="processors">
          <param id="clockSource" type="string">external</param>
       </context>

       <!-- Specific alignment point for processor with ID "processor1" -->
       <context id="processor2">
          <param ns="run" cmd="alignMGTs" id="bx" type="uint">3497</param>
       </context>

       <!-- Specific alignment point for processor with ID "processor2" -->
       <context id="processor2">
          <param ns="run" cmd="alignMGTs" id="bx" type="uint">3498</param>
       </context>

       <!-- In reality, many more contexts & params -->
    </infra>


* ``lookUpTables.xml`` :

  .. code-block:: xml

    <algo id="dummySys">
       <context id="processors">
          <param id=calibLUT" type="vector:uint">
             42, 56, 33, 5684766, 59757
          </param>
       </context>
    </algo>

Only one configuration parameter XML file can be loaded into the SWATCH TS cell at any time; the file can be loaded from the "gatekeeper" sub-panel within the "SWATCH Setup" panel, by specifying the file name and the desired run key. **Note:** command sequences / transitions cannot be run, and state machine cannot be engaged, until an XML file has been loaded.

An example of XML configuration file for a dummy system can be found `here <https://gitlab.cern.ch/cms-cactus/core/swatch/blob/master/swatch/cell/example/test/dummySystem/config.xml>`__

