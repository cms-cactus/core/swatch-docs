
Command sequences
=================

In Processors and DaqTTCManagers, you can define command sequences - lists of your commands that are run sequentially, with the parameters for each command taken from an XML file / from the database (via the data-source-agnostic interface of the ``GateKeeper`` class); in SWATCH versions 0.2 and 0.3 only the XML file backend has been implemented.

A command sequence is added to the processor/DaqTTCManager by calling the ``registerCommandSequence`` in the constructor of the derived (subsystem-specifc) class; this method's arguments must include:

#. the name of the command sequence; and
#. either the first command's ID string, or a reference to the first command.

Extra commands can then be added to the sequence by calling the ``run`` or ``then`` methods of the ``CommandSequence``. The example below shows how to define two command sequences in a processor with three commands - ``commandA``, ``commandB`` and ``commandC`` - one sequence (ID: ``seq1``) that runs command A then B then C, and another sequence (ID: ``seq2``) that just runs command A then C:

.. code-block:: c++

  swatch::action::Command& cmdA = registerFunctionoid<SomeCommandClass>("commandA");
  registerFunctionoid<AnotherCommandClass>("commandB");
  swatch::action::Command& cmdC = registerFunctionoid<AThirdCommandClass>("commandC");

  // Define first sequence, using ID strings of commands
  registerCommandSequence("seq1", "commandA").run("commandB").run(cmdC);

  // Define second sequence, using references to commands
  registerCommandSequence("seq2", cmdC).run(cmdA);



Defining parameter values
-------------------------

The parameter extracted from the XML file / database are arranged in contexts; each context contains a map of ``xdata::Serialisable`` objects identified by strings. Common parameters can be specified once in common contexts; different values can be used for individual processors / DaqTTCManagers through the contexts specific to that individual object.

The rules for which parameter value is used for each command are:

* Look first in the context specific to this processor / DaqTTCManager, then look in the context of common parameter values that are shared by multiple processors / DaqTTCManagers.
* Within each context, look for parameter IDs:

  * ``namespaceID.commandID.parameterName``
  * ``commandID.parameterName``
  * ``parameterName``

* Use the first parameter value found in this search order
* If no parameter is found in this search, the command sequence / transition will not run.

For processors, the following contexts are used: first ``systemID.processorID``; then ``systemID.processorRole``, then ``systemID.processors`` . E.g. for a processor with ID string ``processorA``, and role ``someRole`` in system ``mySystem``, the following order of parameter contexts is used:

* ``mySystem.processorA``
* ``mySystem.someRole``
* ``mySystem.processors``

Similarly, for DaqTTCManager, the following contexts are used: first ``systemID.daqttcMgrID``; then ``systemID.daqttcMgrRole``, then ``systemID.daqttcs``. E.g. for a DaqTTCManager with ID string ``amc13a``, and role ``someAMC13Role`` in system ``mySystem``, the following order of parameter contexts is used:

* ``mySystem.amc13a``
* ``mySystem.someAMC13Role``
* ``mySystem.daqttcs``

Hence, the ``ROLE`` field in the system description file allows you to apply different default parameter values to different sets of processors/DaqTTCManagers from the same system, without having to duplicate entries in the XML file / database contexts.

The ``systemID.processors`` and ``systemID.daqttcs`` contexts allow you to specify default parameter values for all processors / DaqTTCManagers in a system.

When a command is added to a sequence (or when a command / command sequence is added to a transition), you can also define an alias which will be used instead of the default namespace (the sequence/transtion's ID string) when looking for the parameter values for that command. In particular, this is useful if you include the same command twice in the same sequence/transition, but want to use different parameter values the second time it's run, compared with the first
