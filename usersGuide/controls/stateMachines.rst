
Finite state machines
=====================


SWATCH Objects (Processors/DAQTTCManagers) and Systems can implement finite state machines (FSM).
Single-object state machines are meant to encode the state of the hardware/firmware the Object represents and the actions required to move through states. The System FSM primary goals are to encode the state of the System at large and to allow coordinate actions on the System's components.
Although different in implementation, Object and System State machines have a number of elements in common

* At most one FSM per Object/System can be active ("engaged") at any time (the State of the engaged FSM is the "de facto" the state of the Object/System)
* FSMs must be "engaged" (made active) before performing any transition
* States are connected by transitions
* The engaged state machine can be reset to its initial state, as long as no transition is currently running.
* Each FSM has two special states:

  * **Initial** (e.g. "Halted"): Entered upon engaging state machine, or upon resetting the state machine
  * **Error** : Entered if there's an error during any transition

In Object state machines, each transition is a sequence of commands; in System state machines, each transition is one or more steps, where each step is a set of Object transitions executed in parallel.

Each SWATCH Trigger Subsystem must have a common "Run Control SM" for global operations. Runc Control SMs for Processors and Systems are described below in dedicated sections.


Single-object state machines
----------------------------

The single-object state machine is represented by the ``swatch::action::StateMachine`` class (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d8/d59/classswatch_1_1action_1_1_state_machine.html>`__; each transition consists of 0 or more commands executed in sequence, and is represented by the ``swatch::action::Transition`` class (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/dc/d9f/classswatch_1_1action_1_1_transition.html>`__).

Classes that inherit from ``swatch::action::ActionableObject`` (i.e. processors and DaqTTCManagers) can have any number of registered state machines. *N.B.* For the processors and DaqTTCManagers , the run control state machine (i.e. for global running) is already defined in the base class, and the standard states/transitions are already defined; see the next sub-section "Processor run control state machine" for details of the standard run control state machine.

Each state machine is labelled by an ID string. New state machines are registered by calling the ``registerStateMachine`` method in the constructor of the derived processor/DaqTTCManager class (providing the ID string, initial state, and error state):

.. code-block:: c++

  StateMachine& registerStateMachine(const std::string& aId, const std::string& aInitialState, const std::string& aErrorState);

This method returns a reference to the ``swatch::action::StateMachine`` instance for this state machine. You can add states and transitions to the ``StateMachine`` by calling the ``addState`` and ``addTransition`` methods:

.. code-block:: c++

  void addState(const std::string& aId);
  Transition& addTransition(const std::string& aTransitionId, const std::string& aFromState, const std::string& aToState);

The ``addTransition`` method returns a reference to the ``Transition`` instance that represents the transition. When a new single-object transition is created, it does not contain any commands; new commands can be added individually, or as a block from a command sequence, using the ``add`` methods of the ``Transition`` class:

.. code-block:: c++

  Transition& add(Command& aCmd, const std::string& aNamespace="");
  Transition& add(CommandSequence& aSequence);

*N.B.* When all of the commands from a command sequence are added to a transition (i.e. the 2nd method is used), then the parameter values for each command are retrieved from the gatekeeper in exactly the same way as when that command sequence is run.

*N.B.2* When a single command is added to a transition (using the 1st method), then the sequence of searching for parameter values in each context is:

* first look for parameter ID ```namespaceID.commandID.parameterName``,
* then ``commandID.parameterName``,
* then ``parameterName``.

The order in which different tables are used is the same as for command sequences (e.g. for processors: specific context ``mySystem.myProcessorsId``, then role context ``mySystem.myProcessorsRole``, then common context ``mySystem.processors``)


Processor run control state machine
-----------------------------------

The run control FSM itself, its states, and its transitions, are pre-defined in the ``swatch::processor::Processor`` constructor. References to this ``StateMachine`` instance, and the standard transitions are aggregated in the ``swatch::processor::RunControlFSM`` struct (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d0/df3/structswatch_1_1processor_1_1_run_control_f_s_m.html>`__), which can be accessed in the constructor of your processor class through the ``Processor::getRunControlFSM()`` method.

In the subsystem-specific processor classes, you only need to add the commands to the transitions; this should be done in your processor class' constructor.

The standard processor states are:

* Error state
* Halted (initial state)
* Synchronised
* Configured
* Aligned

The standard transitions are:

* **Cold reset** : *Halted* to *Halted*

  * example usage: reboot board with different firmware image

* **Setup** : *Halted* to *Synchronised*

  * Purpose: Reset the board. Configure clocks and TTC blocks. *N.B.* By the end of this transition the board muxt be locked on the correct clock source and BC0

* **Configure** : *Synchronised* to *Configured*

  * Contains bulk of board's configuration - i.e. configure buffers, DAQ block, tx ports, configure algo (load LUTs etc)

* **Align** : *Configured* to *Aligned*

  * Configure rx ports (if necessary) and align inputs

* **Stop** : *Aligned* to *Configured*

  * Takes processor to state from which it can be aligned again (may not run any commands against the hardware in this transition; useful to repeatedly re-align links during testing/commonissioning - i.e. testing that alignment always works)

*N.B.* The exact details run control state machine for the Level-1 upgrades are still under discussion, so there may be changes to the exact definition/multiplicity of transitions and states over the next few months as we understand the requirements of subsystems better.

The ID strings of each state and transition are defined as public static constant data of the ``RunControlFSM`` struct (those beginning with ``kState`` are state IDs, those beginnning with ``kTr`` are transition IDs); the transitions themselves are accessible as public member data of the ``RunControlFSM`` struct.

For a dummy processor class, one might define the commands, sequences and state machines within the derived class's constructor as follows:

.. code-block:: c++

  DummyProcessor::DummyProcessor(const swatch::core::AbstractStub& aStub) :
   // ...
  {
    // 1) Create driver
    // ...
    // ...

    // 2) Commands
    swatch::action::Command& reboot = registerFunctionoid<DummyResetCommand>("reboot");
    swatch::action::Command& reset = registerFunctionoid<DummyResetCommand>("reset");
    swatch::action::Command& cfgBuffers = registerFunctionoid<DummyConfigureBuffersCommand>("configureBuffers");
    swatch::action::Command& cfgTx = registerFunctionoid<DummyConfigureTxCommand>("configureTx");
    swatch::action::Command& cfgRx = registerFunctionoid<DummyConfigureRxCommand>("configureRx");
    swatch::action::Command& cfgDaq = registerFunctionoid<DummyConfigureDaqCommand>("configureDaq");
    swatch::action::Command& cfgAlgo = registerFunctionoid<DummyConfigureAlgoCommand>("configureAlgo");

    // 3) Command sequences
    swatch::action::CommandSequence& cfgSeq = registerCommandSequence("configSeqA",cfgBuffers).then(cfgDaq).then(cfgTx);
    swatch::action::CommandSequence& cfgRxSeq = registerCommandSequence("configRxSeq", cfgRx);

    // 4) State machines
    swatch::processor::RunControlFSM& lFSM = getRunControlFSM();
    lFSM.coldReset.add(reboot);
    lFSM.setup.add(reset);
    lFSM.configure.add(cfgSeq).add(cfgAlgo);
    lFSM.align.add(cfgRxSeq);



Defining additional processor state machines
--------------------------------------------

The run control state machine discussed above must be used for standard global running of the L1 trigger in CMS. But, other single-object state machines might be useful for performing various operations during commissioning, or for (inter-)system tests. These other state machines should also defined in the constructor of your processor class (or in a method called from the constructor). The steps are as follows:

#. Register the state machine using the ``ActionableObject::registerStateMachine`` method; the arguments define:

  * The state machine's ID string
  * The name of the initial state
  * The name of the error state

#. Add the required states, using the ``StateMachine::addState`` method
#. Add the transitions, using the ``StateMachine::addTransition`` method, and hook your commands / command sequences into the transitions

  * *Note:* the ``addTransition`` and ``Transition::add`` methods both return references to the relevant ``Transition`` instance, so the transition can be defined and populated with commands in one line of code!

For example, the code snippet below demonstrates how to define a very simple state machine, with initial state, error state, "FullyConfigured" state, and a transition from initial state to "FullyConfigured":

.. code block:: c++

  // (Within your class' constructor ...)
  swatch::action::StateMachine& otherFSM = registerStateMachine("testFSM", "Halted", "Error");
  otherFSM.addState("FullyConfigured");
  otherFSM.addTransition("configure" , "Halted", "FullyConfigured").add(reset).add(cmdSeq).add(cfgAlgo).add(alignRx);



System state machines
---------------------

The system state machine is represented by the ``swatch::action::SystemStateMachine`` class (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/dc/d09/classswatch_1_1action_1_1_system_state_machine.html>`__). Each transition within the system state machine consists of 0 or more steps executed in sequence, where each step is a set of transitions on child objects (processors / DaqTTCManagers) executed in parallel. For example, if a transition consists of two steps, where the first step is the reset transition for AMC13s, the 2nd step is the reset transition for processors, then this transition will executed as follows:

* **Step 1:** Start the reset transition running on all AMC13s (in parallel)
* Wait for the reset transition to complete on all AMC13s

  * If an error has occurred stop here; otherwise continue

* **Step 2:** Start the reset transition running on all processors (in parallel)
* Wait for the reset transition to complete on all processors

The system transitions are represented by the ``swatch::action::SystemTransition`` class (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d9/daf/classswatch_1_1action_1_1_system_transition.html>`__).

Classes that inherit from ``swatch::action::ActionableSystem`` (i.e. system classes) can have any number of registered state machines. *N.B.* For the system class, the run control state machine (i.e. for global running) is already defined in the ``swatch::system::System`` base class, and the standard states/transitions are already defined; see the next sub-section "System run control state machine" for details of the standard run control state machine.

Like the processor and DaqTTCManager state machines, each system state machine is labelled by an ID string. New state machines are registered by calling the ``registerStateMachine`` method in the constructor of the derived system class (providing the ID string, initial state, and error state):

.. code-block:: c++

  SystemStateMachine& registerStateMachine(const std::string& aId, const std::string& aInitialState, const std::string& aErrorState);

This method returns a reference to the ``swatch::action::SystemStateMachine`` instance for this state machine. You can add states and transitions to the ``SystemStateMachine`` by calling the ``addState`` and ``addTransition`` methods:

.. code-block:: c++

  void addState(const std::string& aId);
  SystemTransition& addTransition(const std::string& aTransitionId, const std::string& aFromState, const std::string& aToState);

The ``addTransition`` method returns a reference to the ``SystemTransition`` instance that represents the transition. When a new transition is created, it does not contain any steps; new steps can be added, using the ``add`` methods of the ``SystemTransition`` class. If all child object transitions in that step have the same ID string, and start from state of the same name in an FSM of the same name, then you can call the ``add`` methods that accept STL collections of pointers to the processors/DaqTTCManagers themselves - i.e:

.. code-block:: c++

  template<class Collection>
  SystemTransition& add (const Collection &aCollection, const std::string &aFromState, const std::string &aTransition);
  template<class Collection>
  SystemTransition& add (const Collection &aCollection, const std::string& aStateMachine, const std::string &aFromState, const std::string &aTransition);

  template<class Iterator>
  SystemTransition& add (Iterator aBegin, Iterator aEnd, const std::string &aFromState, const std::string &aTransition);
  template<class Iterator>
  SystemTransition& add (Iterator aBegin, Iterator aEnd, const std::string& aStateMachine, const std::string &aFromState, const std::string &aTransition);


If the step consists of a set of child object transitions that have different ID strings, that start from different states, or are in FSMs of different names, then you will need to construct a vector of pointers to the transitions themselves, and use the following ``SystemTransition`` method:

.. code-block:: c++

  SystemTransition& add (const std::vector< Transition * > &aTransitions);

In the following sections, there are a few examples of adding steps to transitions. Further documentation of each ``add`` method can be found in the ``SystemTransition`` `doxygen page <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d9/daf/classswatch_1_1action_1_1_system_transition.html>`__.

**N.B.** When all of the the transitions of children (processors & DaqTTCManagers) are executed within a system transition, the parameter values for each underlying command are retrieved from the gatekeeper in exactly the same way as when the single-object transitions are run by themselves.

**N.B.2** When a system state machine is engaged then all of the children involved in its transitions must not have any state machine engaged. When a system state machine is reset/disengaged, then all of the children involved in its transitions must be engaged in the correct state machine, and must not be half-way through executing any transition.


.. _usersGuide-system-run-control-state-machine:

System run control state machine
--------------------------------

The run control FSM itself, its states, and its transitions, are defined in the ``swatch::system::System`` constructor. References to this ``SystemStateMachine`` instance, and the standard transitions are aggregated in the ``swatch::system::RunControlFSM`` struct (doxygen documentation available `here <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d7/d52/structswatch_1_1system_1_1_run_control_f_s_m.html>`__), which can be

The standard system states are:

* Error state
* Halted (initial state)
* Synchronised
* Configured
* Aligned
* Running
* Paused

The standard transitions are:

* **Cold reset** (*Halted* to *Halted*). Usage:

  * Reboot DaqTTCManagers and processors with different firmware image

* **Setup** (*Halted* to *Synchronised*). Usage:

  #. Reset AMC13s
  #. Configure AMC13s' TTC blocks
  #. Execute Processors' 'Setup' transition (see above)
  #. Enable AMC-AMC13 backplane links. Configure AMC13-DAQ link.

* **Configure** (*Synchronised* to *Configured*). Usage:

  * Execute 'Configure' transition on all Processors (see above)

* **Align** (*Configured* to *Aligned*). Usage:

  * Execute 'Align' transition on all Processors (see above)

* **Start** (*Aligned* to *Running*). Usage:

  * Execute 'Start' transition on all AMC13s, *and* on all processors

* **Pause** (*Running* to *Paused*). Usage:

  * Execute 'Pause' transition on all AMC13s

* **Resume** (*Paused* to *Running*). Usage:

  * Execute 'Resume' transition on all AMC13s

* **Stop** (*Aligned* / *Running* / *Paused* to *Configured*). Usage:

  * Execute 'Stop' transition on all AMC13s (if *not* in ``Aligned`` state), *and* on all processors

**N.B.** In your system, you may have to perform actions on some processors/AMC13s before others, so you may have different numbers of steps in your transitions from those implied above. The transitions are defined in the subsystem's system class to provide this flexibility.

**N.B.2:** The exact details run control state machine for the Level-1 upgrades are still under discussion, so there may be changes to the exact definition/multiplicity of transitions and states over the next few months as we understand the requirements of subsystems better.

In order to define how the system state machine's transitions map onto the transitions of the processors and DaqTTCManagers, you will need to create a subsystem-specific system class; this subsystem-specific system class must:

* Inherit from ``swatch::system::System``
* Be registered with the SWATCH factories

  * i.e. like with the processor class, the ``cpp/cc`` file for your subsystem should contain the following line (relevant header ``swatch/core/Factory.hpp`` ): ``SWATCH_REGISTER_CLASS(exampleSubsystem::MySystem)``

You can then switch to using your subsystem-specific system class by adding changing the value for the system-level ``creator`` element in your system description XML file to the name of your class - i.e:

.. code-block:: xml

  <system id="mySystem">
    <creator>exampleSubsystem::MySystem</creator>

In the constructor of your subsystem-specific system class, you just need to add the transitions of the child objects (processors and DaqTTCManagers) into the system's transitions. **Remember:** Each system-level transition consists of 0 or more steps; each step is a set of transitions on processors/DaqTTCManagers. The steps are executed in sequence (i.e. each step only starts running after all transitions in the previous step have completed); however the child transitions that make up a single step are run in parallel.

You can access the ``swatch::system::RunControlFSM`` struct in the constructor of your subsystem-specific system class through the ``System::getRunControlFSM()`` method. The ID strings of each state and transition are defined as public static constant data of the ``RunControlFSM`` struct (those beginning with ``kState`` are state IDs, those beginnning with ``kTr`` are transition IDs); the transitions themselves are accessible as public member data of the ``RunControlFSM`` struct.

For a dummy system class, one would define the system-level run control state machine as follows:

.. code-block:: c++

  DummySystem::DummySystem(const swatch::core::AbstractStub& aStub) :
    swatch::system::System(aStub)
  {
    typedef processor::RunControlFSM ProcFSM_t;
    typedef dtm::RunControlFSM DaqTTCFSM_t;

    system::RunControlFSM& fsm = getRunControlFSM();
    fsm.coldReset.add(getDaqTTCs(), DaqTTCFSM_t::kStateInitial, DaqTTCFSM_t::kTrColdReset)
                 .add(getProcessors(), ProcFSM_t::kStateInitial, ProcFSM_t::kTrColdReset);

    fsm.setup.add(getDaqTTCs(), DaqTTCFSM_t::kStateInitial, DaqTTCFSM_t::kTrClockSetup)
             .add(getProcessors(), ProcFSM_t::kStateInitial, ProcFSM_t::kTrSetup)
             .add(getDaqTTCs(), DaqTTCFSM_t::kStateClockOK, DaqTTCFSM_t::kTrCfgDaq);

    fsm.configure.add(getProcessors(), ProcFSM_t::kStateSync, ProcFSM_t::kTrConfigure);

    fsm.align.add(getProcessors(), ProcFSM_t::kStateConfigured, ProcFSM_t::kTrAlign);

    fsm.start.add(getProcessors(), ProcFSM_t::kStateAligned, ProcFSM_t::kTrStart)
             .add(getDaqTTCs(), DaqTTCFSM_t::kStateConfigured, DaqTTCFSM_t::kTrStart);

    fsm.pause.add(getDaqTTCs(), DaqTTCFSM_t::kStateRunning, DaqTTCFSM_t::kTrPause);

    fsm.resume.add(getDaqTTCs(), DaqTTCFSM_t::kStatePaused, DaqTTCFSM_t::kTrResume);

    fsm.stopFromAligned.add(getProcessors(), ProcFSM_t::kStateAligned, ProcFSM_t::kTrStop);

    fsm.stopFromRunning.add(getDaqTTCs(), DaqTTCFSM_t::kStateRunning, DaqTTCFSM_t::kTrStop)
                       .add(getProcessors(), ProcFSM_t::kStateRunning, ProcFSM_t::kTrStop);

    fsm.stopFromPaused.add(getDaqTTCs(), DaqTTCFSM_t::kStatePaused, DaqTTCFSM_t::kTrStop)
                      .add(getProcessors(), ProcFSM_t::kStateRunning, ProcFSM_t::kTrStop);
  }



If some processors had to be aligned before other processors could align, then this could be acheived as follows:

.. code-block:: c++

  std::vector<swatch::processor::Processor*> processorSet1;
  // ... fill up processorSet1 ...
  std::vector<swatch::processor::Processor*> processorSet2;
  // ... fill up processorSet2 ...
  fsm.align
            // Step 1: Align processor set 1 (in parallel)
            .add(processorSet1, tProcFSM::kStateConfigured, tProcFSM::kTrAlign)
            // Step 2: Align processor set 2 (in parallel)
            .add(processorSet2, tProcFSM::kStateConfigured, tProcFSM::kTrAlign);


**N.B.** Don't forget to register your subsystem specific system class to the SWATCH factories, as follows:

.. code-block:: c++

  SWATCH_REGISTER_CLASS(mySubsystem::MySystem);



Defining additional system-level state machines
-----------------------------------------------

The run control state machine discussed above must be used for standard global running of the L1 trigger in CMS. But, other system-level state machines might be useful for performing various operations during commissioning, or for (inter-)system tests. These other state machines should also defined in the constructor of your system class (or in a method called from the constructor). The steps are as follows:

#. Register the state machine using the ``ActionableSystem::registerStateMachine`` method; the arguments define:

   * The state machine's ID string
   * The name of the initial state
   * The name of the error state

#. Add the required states, using the ``SystemStateMachine::addState`` method

#. Add the transitions, using the ``SystemStateMachine::addTransition`` method, and hook the transitions of your processors and DaqTTCManagers into the system transitions

  * *Note:* the ``addTransition`` and ``SystemTransition::add`` methods both return references to the relevant ``SystemTransition`` instance, so the transition can be defined and populated in a minimal amount of code!

For example, the code snippet below demonstrates how to define a very simple system state machine for parallel running, based on the standard run control state machines for processors and DaqTTCManagers. Detailed specification:

* States: initial; error; stand-by; and running
* Transitions:

  * **setup** : Initial state to stand-by state

    #. Resets AMC13s
    #. Resets processors
    #. Configures DAQ blocks of AMC13s
    #. Configures processors

  * **run** : stand-by state to running state

    #. Aligns rx ports of processors
    #. Starts AMC13 DAQ

corresponding code:

.. code-block:: c++

  // Register the state machine, and define all states
  swatch::action::SystemStateMachine& parFSM = registerStateMachine(, "Halted", "Error");
  parFSM.addState("StandBy");
  parFSM.addState("Running");

  // Typedefs and local variable to shorten following code for transition definitions
  typedef swatch::processor::RunControlFSM ProcFSM_t;
  typedef swatch::dtm::RunControlFSM DaqTTCFSM_t;
  const std:string& rcId = swatch::system::RunControlFSM::kId;

  // Setup: reset DTManagers and Processors
  parFSM.addTransition("setup", "Halted", "StandBy" )
    // Reset DTManagers
    .add(getDaqTTCs(), rcId, DaqTTCFSM_t::kStateInitial, DaqTTCFSM_t::kTrClockSetup)
    // Reset Processors
    .add(getProcessors(), rcId, ProcFSM_t::kStateInitial, ProcFSM_t::kTrSetup)
    // Configure DTM DAQ
    .add(getDaqTTCs(), rcId, DaqTTCFSM_t::kStateClockOK, DaqTTCFSM_t::kTrCfgDaq)
    // Preconfigure all processors
    .add(getProcessors(), rcId, ProcFSM_t::kStateSync, ProcFSM_t::kTrConfigure)
  ;

  parFSM.addTransition("start", "StandBy", "Running")
    // Align processors' rx ports
    .add(getProcessors(), rcId, ProcFSM_t::kStateConfigured, ProcFSM_t::kTrAlign)
    // Start DTMs
    .add(getDaqTTCs(), rcId, DaqTTCFSM_t::kStateConfigured, DaqTTCFSM_t::kTrStart)
  ;


