

Controls
========

There are three concepts of actions in SWATCH:

* Commands: One shot action - e.g. reset board, configure links
* Command sequences: Multiple commands executed in succession
* Operations: FSMs (Finite State Machines); each transition is typically a single command or command sequence

These concepts, their implementation in C++, and their configuration through XML tags in files or the database, are explained in the following sections:

.. toctree::
   :maxdepth: 2

   commands
   commandSequences
   stateMachines
   definingParameterValues
   exclusionFromSystemLevelActions

