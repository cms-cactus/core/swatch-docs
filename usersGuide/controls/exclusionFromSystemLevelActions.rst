
Excluding specific processors and AMC13s from system-level actions
==================================================================

Under certain scenarios, we will want to temporarily exclude certain processors and/or AMC13s from system-level actions (e.g. transitions) at runtime, without recompiling our code. For example, the excluded boards may be malfunctioning, or we may want to perform special test. In such a scenario:

* We *disable* the processors/AMC13s that should not take part in system-level actions.
* All other boards are said to be *enabled*.

You can specify which boards to disable in your XML configuration files, through the ``disable`` tag, which can be used in the sub-config module files, as a child of the ``module`` tag. The ``disable`` tag's ``id`` attribute specifies the ID path of the board that you want to disable. For example, in a system of ID ``mySystem``, in order to disable a processor with ID ``processor2`` and AMC13 with ID ``AMC13a``, you would include the following sub-config module file in your configuration:

.. code-block:: xml

  <run-settings id="mySystem">
      <disable id="myProcessor" />
      <disable id="myAMC13a" />
  </run-settings>


The **enable/disable flag for each processor and AMC13 is automatically set when a system state machine is engaged or reset**; specifically, when you engage or reset a system state machine the system FSM disables all of the processors/AMC13s that are specified with the disable tags in the XML configuration file
