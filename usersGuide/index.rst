.. testdrive documentation master file, created by
   sphinx-quickstart on Sat Jan 23 17:27:43 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Guide for subsystem developers
==============================

Welcome to the SWATCH user's guide!

Contents:

.. toctree::
   :maxdepth: 2

   intro
   install_P5
   install_not_P5
   cell
   processorSpecialisation
   controls/index
   monitoring
   maskableObjects
   systemDescription



