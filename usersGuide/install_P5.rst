Installation at P5/904
======================

At P5/904, trigger packages are installed for you using `Puppet <https://puppet.com/open-source/#osp>`_.

The sysadmins manage the puppet configuration for your machine. 

To enable a trigger profile, or upgrade to a new one (e.g. when switching major XDAQ version), submit a ticket to
`the sysadmins <https://its.cern.ch/jira/projects/CMSONS/>`__.  
Do not forget to include their hostnames in the request.

**IMPORTANT**: **Do not** use any method described for machines outside P5. Custom YUM repositories break the puppet configuration and are in any case inaccessible inside the CMS network.

Common SWATCH plugin for the AMC13 & MP7
----------------------------------------

The common AMC13 and MP7 SWATCH plugins are installed by Puppet as well, along with the appropriate version of the AMC13/MP7 driver software.

**N.B.** Version 1.3.0 of the MP7 SWATCH plugin requires version 2.6.0 of the MP7 driver software.
