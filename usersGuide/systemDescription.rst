
.. _usersGuide-system-description:

System description
==================

In SWATCH, each trigger system (calo layer-1, calo layer-2, uGT, uGMT, ...) is described by listing the important details of its processors, optical links, DaqTTCManagers, and crates in an XML file / database. A SWATCH system that's described in this way can then be created by the SWATCH framework's factory classes, through the generic SWATCH-TS panels.

In order for SWATCH to create the instances of the subsystem-specific C++ classes for processors and systems, each subsystem-specific processor/DaqTTCManager/system class must be registered via the ``SWATCH_REGISTER_CLASS`` macro (as explained earlier in step 2 of the processor section) - e.g. for the processor class ``examplesubsystem::MyProcessor`` , its cpp file should include the following lines:

.. code-block:: c++

  #include "swatch/core/Factory.hpp"

  SWATCH_REGISTER_CLASS( examplesubsystem::MyProcessor );


With this macro used in the class' cpp file, the subystem-specifc processor/DaqTTCManager/system class can be created by the SWATCH framework by setting that object's "creator" value to the class name (including namespaces) in the XML file / database entries that describe the system.

The SWATCH TS cell framework provides each cell with a webpage that can be used to load a system from an XML file (or database key), as explained earlier. The rest of this section describes all of the fields expected in the XML file; once you start adapting the example XML files to describe your own system, the easiest way to test your XML file will be to try to load that file into your TS cell, using the ``SWATCH Setup`` control panel (found in the menu on the left of the cell's webpage).

The XML file describing an example system - used to control a dummy test system composed of several processors and AMC13s - can be found here: https://gitlab.cern.ch/cms-cactus/core/swatch/blob/master/swatch/cell/example/test/dummySystem/dummySystem.xml

The XML file contains one top-level element - ``system``; its ``id`` attribute specifies the system's ID string (e.g. ``calol2``), and its contents are organised into sections, with the following child tags:

* ``creator`` (mandatory): Name of the class used to represent this system; this class must directly/indirectly inherit from ``swatch::system::System``

  * To set up your system-level state machine, each subsystem should eventually have their own system class that inherits from ``swatch::system::System``
  * *N.B.* However, until the subsystem-specific system class has been written, you can use the ``swatch::system::System`` class - i.e. ``<creator>swatch::system::System</creator>``

* ``processors`` (optional): Can have one or more ``processor`` child tags, each of which describes an individual processor.
* ``daqttc-mgrs`` (optional): Can have one or more ``daqttc-mgr`` child tags, each of which describes an individual DaqTTCManager(detailed sub-structure explained below).
* ``links`` (optional): Can have one or more ``link`` child tags, each of which describes an idividual optical link connecting processors within this system (intra-system links).
* ``connected-feds`` (optional): Can have one or more ``connected-fed`` child tags, describing the mapping of external FEDs (e.g. from ECAL, HCAL, muon subdetectors, or other L1 subsystems) onto this system's input ports.

  * **N.B.** This FED-input port mapping is used to automatically mask input ports from when FEDs are not included in a CMS run.

* ``excluded-boards`` (optional): Can have one or more ``exclude`` child tags specifying the processors / AMC13 managers that should not be constructed

For example, a system XML looks something like:

.. code-block:: xml

  <system id="mySystem">
    <creator>MySystemClass</creator>
    <crates>
      <!-- one or more crate tags -->
    </crates>
    <processors>
      <!-- one or more processor tags -->
    </processors>
    <daqttcs>
      <!-- one or more daqttc tags -->
    </daqttcs>
  </system>


The structure within the ``processor`` , ``daqttc-mgr``, ``link``, and ``connected-fed`` tags is described in the following sections


Processor description
---------------------

Each of the ``processor`` XML tags must have:

* One attribute:

  * ``id`` : An ID string (no dots - "." - and ideally no spaces); must be different for each processor/DaqTTCManager/crate/link in this system

* One of each of the following child tags (the values are specified as the XML tag's contents):

  * ``creator`` : Name of class used to represent this processor (including namespaces); this class must directly/indirectly inherit from ``swatch::processor::Processor``.
  * ``hw-type`` : String recording hardware type of this processor
  * ``role`` : Identifies a group of processors which use one or more common parameter values for commands; allows such common parameter values to be specified without having to duplicate entries in the XML file / database contexts
  * ``uri`` : URI string for communicating with the processor
  * ``address-table`` : Location of address table file for this processor
  * ``crate`` : ID string of the crate that this processor is in.
  * ``slot`` : Number of the crate slot that this processor is in.

* Zero or more of the following child tags describing optical I/O ports:

* ``rx-port`` : Describe an input optical port; each ``rx-port`` tag must have two attributes:

  * ``name`` : An ID string (no dots - "." - and ideally no spaces); must be different for each input/output port of the same processor.
  * ``pid`` : An ID number for this port

  Multiple ports can be specified in a single ``rx-port`` element by using the slice syntax in both attributes

* ``tx-ports`` : Same as ``rx-ports`` , but for output ports

E.G:

.. code-block:: xml

  <processor id="mp7_slot2">
    <creator>swatch::mp7::MP7NullAlgoProcessor</creator>
    <hw-type>MP7 XE</hw-type>
    <role>procRoleA</role>
    <uri>chtcp-2.0://controlhub-hostname:10203?target=processor-dns-name:50001</uri>
    <address-table>file:///opt/cactus/etc/mp7/addrtab/xe_v1_7_7_rc1/mp7xe_infra.xml<uri>
    <crate>Schroff crate 2</crate>
    <slot>2</slot>
    <rx-port name="Rx[00:72]" pid="[00:72]"/>
    <tx-port name="Tx[00:72]" pid="[00:72]"/>
  </processor>


Each of these fields corresponds to some member data in the ``swatch::processor::ProcessorStub`` class, which can be accessed from within the subsystem-specific processor class constructors via the ``getStub()`` method (that's inherited from ``swatch::processor::Processor``); thus, the ``ProcessorStub`` member data is used to instantiate the subsystem-specific processor class such that each instance talks to the right board, and has the correct composition of output & input port objects. The doxygen page for the ``ProcessorStub`` class can be found at http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/dd/d98/classswatch_1_1processor_1_1_processor_stub.html


AMC13 description
-----------------

Each of the ``daqttc-mgr`` XML tags must have:

* One attribute:

  * ``id`` : An ID string (no dots - "." - and ideally no spaces); must be different for each processor/DaqTTCManager/crate/link in this system

* One of each of the following child tags (the values are specified as the XML tag's contents):

  * ``creator`` : Name of class used to represent this DaqTTCManager (including namespaces); this class must directly/indirectly inherit from ``swatch::dtm::DaqTTCManager``.
  * ``role`` : Identifies a group of processors which use one or more common parameter values for commands; allows such common parameter values to be specified without having to duplicate entries in the XML file / database contexts
  * ``crate`` : ID string of the crate that this DaqTTCManager is in
  * ``slot`` : Number of the slot that this DaqTTCManager is in (for AMC13s, this will be 13)
  * ``fed-id`` : FED ID number.

* Two of each of the following child tags (the values are specified as the XML tag's contents), one with ``id`` attribute ``t1``, the other with ``id`` attribute ``t2``:

  * ``uri`` : URI for communicating with tongue 1 or 2
  * ``address-table`` : Location of address table file for tongue 1 or 2

E.G:

.. code-block:: xml

  <daqttc-mgr id='amc13_1'>
    <creator>swatch::amc13::AMC13Manager</creator>
    <role>procRoleA</role>
    <crate>Schroff crate 2</crate>
    <slot>13</slot>
    <uri id='t1'>chtcp-2.0://controlhub-hostname:10203?target=amc13-dns-name:50001</uri>
    <address-table id='t1'>file:///opt/cactus/etc/amc13/AMC13XG_T1.xml</address-table>
    <uri id='t2'>chtcp-2.0://controlhub-hostname:10203?target=amc13-dns-name:50001</uri>
    <address-table id='t2'>file:///opt/cactus/etc/amc13/AMC13XG_T2.xml</address-table>
    <fed-id>1234</fed-id>
  </daqttc-mgr>


Each of these fields corresponds to some member data in the ``swatch::processor::DaqTTCStub`` class, which can be accessed from within the specialised DaqTTCManager class constructors via the ``getStub()`` method (that's inherited from ``swatch::dtm::DaqTTCManager``); thus, the ``DaqTTCStub`` member data is used to instantiate the specialised DaqTTCManager class such that each instance talks to the right board, and has the correct FED ID. The doxygen page for the ``DaqTTCStub`` class can be found at http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d3/d99/classswatch_1_1dtm_1_1_daq_t_t_c_stub.html


.. _usersGuide-system-description-excluded-boards:

Excluded boards
---------------

This ``excluded-boards`` section specifies a set of processors / AMC13 managers that should not be constructed. This functionality is useful in case the processor/manager constructor contacts the hardware (as the MP7 and AMC13 classes do), but some slots described in the ``processors`` / ``daqttc-mgrs`` collections are temporarily not populated. The only valid child tag in this section is the ``exclude`` tag; each ``exclude`` element must have an ``id`` attribute, specifying the ID of a board whose C++ control class should not be constructed.


Example:

.. code-block:: xml

  <excluded-boards>
    <exclude id='someAbsentProcessor'/>
  </excluded-boards>


Crate description
-----------------

Each of the ``crate`` XML tags must have:

* One attribute:

  * ``id`` : An ID string for the crate (no dots - "." - and ideally no spaces); must be different for each processor/DaqTTCManager/crate/link in this system.

* One of each of the following child tags (the values are specified as the XML tag's contents):

  * ``location`` : The physical location of the crate (a string)
  * ``description`` : A slightly more verbose description for the crate (if wanted)

E.G:

.. code-block:: xml

  <crate id='crateA'>
    <location>Point5, S2D10-43</location>
    <description>Our cool crate that ... </description>
  </crate>


This information is converted by the SWATCH framework into a ``swatch::system::CrateStub``, which is used to create an instance of ``swatch::system::Crate``, accessible from the `API <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d5/df2/classswatch_1_1system_1_1_system.html>`__ of the ``swatch::system::System`` class. No subsystem specialisation is allowed (currently) for this class.


Intra-system link description
-----------------------------

Each of the ``link`` XML tags must have:

* One attribute:

  * ``id`` : An ID string for the link (no dots - "." - and ideally no spaces); must be different for each processor/DaqTTCManager/crate/link in this system.

* One of each of the following child tags (the values are specified as the XML tag's contents):

  * ``from``: ID string of the source processor
  * ``tx-port``: ID string of the source port
  * ``to``: ID string for the destination processor
  * ``rx-port``: ID string of the destination port

i.e. the link description object looks something like:

.. code-block:: xml

  <link id='link_A1C_[00:10]'>
    <from>procA1</from>
    <tx-port>Tx[00:10]</tx-port>
    <to>procC</to>
    <rx-port>Rx[00:10]</rx-port>
  </link>


This information is converted by the SWATCH framework into a ``swatch::system::LinkStub``, which is used to create an instance of ``swatch::system::Link``, accessible from the `API <http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/d5/df2/classswatch_1_1system_1_1_system.html>`__ of the ``swatch::system::System`` class. No subsystem specialisation is allowed (currently) for this class.


.. _usersGuide-system-description-connected-FEDs:

Connected FEDs
--------------

.. Each of the objects describing external connected FEDs in the JSON file should contain the a list of FED ID to input port mapping objects. Each FED-port mapping object has the following attributes:

Each of the ``connected-fed`` XML tags must have:

* One attribute:

  * ``id`` : The FED's ID number (as used for central DAQ purposes)

* One oor more of the following child tag

  * ``port``: The ID path of an input port that this external FED is connected to - i.e. ``processorID.inputPorts.portID`` - is specified as the ``id`` attribute.

The example below contains the XML ``connected-feds`` tag describing a system in which ...

* FED 999 is connected to input ports ``Rx00`` to ``Rx09`` inclusive on processor ``procA1``, and input port ``Rx00`` of processor ``procB``
* FED 1001 is connected to input ports ``Rx10`` to ``Rx19`` inclusive on processor ``procA1``

.. code-block:: xml

  <connected-feds>
    <connected-fed id='999'>
      <port id='procA1.inputPorts.Rx[00:10]'/>
      <port id='procB.inputPorts.Rx00' />
    </connected-fed>
    <connected-fed id='1001'>
      <port id='procA1.inputPorts.Rx[10:20]' />
    </connecte-fed>
  </connected-feds>
