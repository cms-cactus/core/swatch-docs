
Monitoring
==========

The main objects in a SWATCH system are "monitorable" (inherit from ``swatch::core::MonitorableObject``) - specifically: DaqTTCManagers (AMC13s); processors; processor interfaces (TTC, readout, algorithm, input port, output port); and systems.

This means that you can register one/more "metrics" (that inherit from ``swatch::core::Metric``) in each of these objects. A "metric" is:

* monitoring data (e.g. int, bool, ...) that is retrieved from hardware or calculated from other metrics
* ... labelled with an ID string
* ... and has associated limits defining whether the metric's value is "Good", indicates an error, or should be flagged up as a warning

The ``swatch::core::SimpleMetric<DataType>`` class template provides a default implementation of the metric interface for a given data type, corresponding to a item of monitoring data that's read from the hardware; the values of these metrics are must be set within the ``void retrieveMetricValues()`` method of the subsystem-specific processor/interface class.

The values of an object's metrics can be updated external code (e.g. the TS cell) by calling the ``void updateMetrics()`` method (inherited from ``swatch::core::MonitorableObject``); this method calls the subsystem-specific implementation of ``void retrieveMetricValues()``, handles any exceptions thrown by that method, and keeps track of whether any metrics' values weren't updated in that method (in which case the status of those metrics is set to ``swatch::core::kUnknown``).

The current status of any monitorable object can be queried through the ``getStatus()`` method, which "AND"s together the status of all that object's metrics, and all its child objects that are monitorable.

The advantages of including monitorable data in the SWATCH ``MonitorableObject`` / ``Metric`` framework are that:

* All monitorable quantities will be visible to the SWATCH TS cell framework through a homogenous interface, allowing for implementation of framework functions that:

  * Spawn a thread in the subsystem TS cell that iterates over all metrics, updating their values periodically
  * Identify what part of a system/processor is in error, and report this information in a homogenous way
  * Populate flashlists, and interface to alarming systems.

* SWATCH provides the capability for including both common metrics (quantities that should be monitored by all subsystems), and subsystem-specific metrics


**Step 1: Retrieve and store values of common metrics**

Some metrics are already registered in the generic processor and processor interface (TTC/readout/optical port) classes; these are intended to represent monitorable quantities are common to all subsystems, and their values must be read from hardware in the ``void retrieveMetricValues()`` method.

The common metrics are (all accessible as protected member data of the interface classes):

* Processor:

  * Firmware version: ``swatch::core::SimpleMetric<uint64_t> mMetricFirmwareVersion;``

     * No warning/error conditions

* TTC interface:

  * Bunch, event and orbit counters:

    * ``swatch::core::SimpleMetric<uint32_t>& mMetricBunchCounter;``
    * ``swatch::core::SimpleMetric<uint32_t>& mMetricL1ACounter;``
    * ``swatch::core::SimpleMetric<uint32_t>& mMetricOrbitCounter;``
    * No warning/error conditions

  * Is 40MHz clock locked ? ``swatch::core::SimpleMetric<bool>& mMetricIsClock40Locked;``

    * Error if equals ``false``

  * Has 40MHz clock stopped ? ``swatch::core::SimpleMetric<bool>& mMetricHasClock40Stopped;``

    * Error if equals ``true``

  * Is BC0 locked ? ``swatch::core::SimpleMetric<bool>& mMetricIsBC0Locked;``

    * Error if equals ``false``

  * Single-bit error counter: ``swatch::core::SimpleMetric<uint32_t>& mMetricSingleBitErrors;``

    * Error if non-zero; no warning condition

  * Double-bit error counter: ``swatch::core::SimpleMetric<uint32_t>& mMetricDoubleBitErrors;``

    * Error if non-zero; no warning condition

* Readout interface:

  * TTS state ``swatch::core::SimpleMetric<uint32_t>& mMetricTTS;``

    * Error if equals false

  * Is AMC DAQ core ready? ``swatch::core::SimpleMetric<bool>& mMetricAMCCoreReady;``

    * Error if equals ``2``
    * Warning if doesn't equal ``8``

  * Event counter ``swatch::core::SimpleMetric<uint32_t> mMetricEventCounter;``


* Input port:

  * Is it locked ? ``swatch::core::SimpleMetric<bool>& mMetricIsLocked;``

    * Error if equals ``false``

  * Is it aligned ? ``swatch::core::SimpleMetric<bool>& mMetricIsAligned;``

    * Error if equals ``false``

  * CRC error counter: ``swatch::core::SimpleMetric<uint32_t>& mMetricCRCErrors;``

    * Error if non-zero; no warning condition

* Output port:

  * Is port operating correctly? ``swatch::core::SimpleMetric<bool>& mMetricIsOperating;``

    * Error if equals ``false``

The values of these metrics should be read from hardware in the ``void retrieveMetricValues()`` method, and hooked into the SWATCH framework by calling the the ``setMetricValue<T>(swatch::core::SimpleMetric<T>& metric, T newValue)`` method; for example, in the case of a subsystem-specific input port class ``MyInputPort`` (with hardware driver ``MyHardwareDriver`` used to talk to the hardware), one might implement the ``retrieveMetricValues()`` method as follows:

.. code-block:: c++

  void MyInputPort::retreiveMetricValues() {
    MyHardwareDriver& driver = getDriver();

    // Retrieve monitoring data from hardware ...
    bool isLocked = driver.isInputPortLocked(portIndex);
    bool isAligned = driver.isInputPortAligned(portIndex);
    uint32_t crcErrorCounter = driver.getCRCErrors(portIndex);

    // Set the values of the SWATCH metrics to the values that were read from hardware
    setMetricValue<>(mMetricIsLocked, isLocked);
    setMetricValue<>(mMetricIsAligned, isAligned);
    setMetricValue<>(mMetricCRCErrors, crcErrorCounter);
  }


**Step 2: Register subsystem-specific metrics**


In the subsystem classes for processors and processor component interfaces (TTC, readout, input port, output port, and algorithm), you can add subsystem-specific metrics, which will then be accessible from the generic SWATCH API, and updated along with that object's common metrics. The new metrics are registered in the constructor of the subsystem-specific class, by calling one of the ``registerMetric`` methods, and their values must be set in the subsystem class' implementation of the ``retrieveMetricValues()`` method (in the same way as the values of the common metrics are set).

For example, a ``uint16_t`` counter for an input port - with "good" status for value of zero, error if the value is above 8, and warning otherwise - could be registered in the ``MyInputPortClass`` as follows:

* Add a reference to that type of metric as member data:


.. code-block:: c++

  class MyInputPort {
    // ...
    // ...
    // ...

    swatch::core::Metric<uint16_t>& mMetricMySpecialCounter;
  };


* Register that metric in the constructor:

.. code-block:: c++

  MyInputPort::MyInputPort ( ... ) :
    swatch::processor::InputPort(stub),
    // ...
    mMetricMySpecialCounter( registerMetric<uint16_t>("specialCounter", swatch::core::GreaterThanCondition<uint16_t>(8), swatch::core::RangeCondition<uint32_t>(1,8)) )
  {
    // ...
  }


* Read and store that metric's value in the ``retrieveMetricValues()`` method

.. code-block:: c++

  void MyInputPort::retreiveMetricValues() {
    MyHardwareDriver& driver = getDriver();

    // Retrieve monitoring data from hardware ...
    bool isLocked = driver.isInputPortLocked(portIndex);
    bool isAligned = driver.isInputPortAligned(portIndex);
    uint32_t crcErrorCounter = driver.getCRCErrors(portIndex);
    uint16_t specialErrorCounterValue = driver.getSpecialErrors(portIndex);

    // Set the values of the SWATCH metrics to the values that were read from hardware
    setMetricValue<>(mMetricIsLocked, isLocked);
    setMetricValue<>(mMetricIsAligned, isAligned);
    setMetricValue<>(mMetricCRCErrors, crcErrorCounter);
    setMetricValue<>(mMetricMySpecialCounter, specialErrorCounterValue);
  }


For more ideas on how to register metrics of different types, or with different warning/error conditions, please take a look at how the common metrics are registered - e.g:

* TTC interface common metrics: https://gitlab.cern.ch/cms-cactus/core/swatch/blob/master/swatch/processor/src/common/TTCInterface.cpp
* Input port common metrics: https://gitlab.cern.ch/cms-cactus/core/swatch/blob/master/swatch/processor/src/common/Port.cpp


.. _usersGuide-monitoring-complex-metrics:

Complex metrics
^^^^^^^^^^^^^^^


The section above described how to register and set the values of simple metrics, whose values correspond to individual items of monitoring data that are read from hardware. The SWATCH framework provides a different interface - based on the template class ``swatch::core::ComplexMetric<DataType>`` - for metrics whose values are not directly retrieved from hardware, but instead calculated from the values of other metrics; for example:

 * The total sum of CRC mismatches on multiple (unmasked) input ports
 * The number of (unmasked) input ports that are not aligned within one processor, or across the whole system

Just like the simple metrics described in the previous section, each complex metric has a value, is labelled by an ID string, and can have conditions that determine if the value is "good", or indicates an error/warning. The only difference betwen a complex metric and those described above is how their values are set.

Complex metrics can be registered using the one of the ``registerComplexMetric`` methods; in these methods you must specify:

 * The complex metric's ID string.
 * The set of other metrics ('input metrics') from which this complex metric's value can be calculated.

   * Note: A complex metric's value can depend on the values of other complex metrics and/or on the values of 'simple' metrics.

 * The function that calculates this complex metric's value from the values of the input metrics

   * This function should have signature ``const DataType* calculateComplexMetricValue(const std::vector<swatch::core::MetricSnapshot>& )``
   * This function is called by the framework whenever the value of any of the input metrics is updated, and its argument contains one ``swatch::core::MetricSnapshot`` instance for each of the input metrics; the latest value of each input metric can be accessed using the ``getValue()`` method of the ``MetricSnapshot`` class
   * The returned value should be a pointer to the complex metric's new value (heap allocated), or ``NULL`` if this value is not known (e.g. if the values of the input metrics are unknown)
   * N.B. When a new value is calculated, the SWATCH framework will take care of deleting the complex metric's pre-existing value

You can also provide the complex metric with a filter function, which can be used to filter out metrics from being given to the "calculate" function, based on the properties of their parent object. E.g. by defining a filter function, you can exclude the metrics of masked ports from port-related complex metrics.

 * The filter function must have signature: ``bool myFilterFunction(const swatch::core::MonitorableObject& )``
 * The filter function is called before the complex metric's "calculate" function is called, once for each input metric.

   * The argument to the filter function is the input metric's parent (e.g. the ``InputPort`` instance).
   * If the filter function returns ``true`` then that input metric is included in the ``vector<swatch::core::MetricSnapshot>`` argument of the "calucate" function; if it returns ``false`` then that input metric excluded from the "calculate" function's argument.


Example code for implementing complex metrics in some typical uses cases is given below ...

**Example 1:** Calculate the total number of CRC errors across all unmasked input ports in one processor:

.. code-block:: c++

  bool filterOutMaskedPorts(const core::MonitorableObject& aObj)
  {
    // Ensures that only metrics within UN-masked input ports are included in the metric value calculation
    return !dynamic_cast<const processor::InputPort&>(aObj).isMasked();
  }

  const uint32_t* sumUpCRCErrors(const std::vector<core::MetricSnapshot>& aSnapshots)
  {
    uint32_t lResult = 0;

    for (auto lIt=aSnapshots.begin(); lIt != aSnapshots.end(); lIt++) {

      if (lIt->isValueKnown())
        (lResult) += lIt->getValue<uint32_t>();
      // If the value of one of the ports' CRC error counters is unknown, then the sum is not known either
      else
        return NULL;
    }

    return new uint32_t(lResult);
  }

  MyCoolProcessor::MyCoolProcessor(const swatch::core::AbstractStub& aStub) :
    Processor(aStub),
    mDriver(new DummyProcDriver())
  {
    // First, must create all of the input ports
    // ...
    // ...

    // Now register the "totalCRCerrors" complex metric
    std::vector<core::AbstractMetric*> lCRCErrorMetrics;
    for (auto lIt=getInputPorts().getPorts().begin(); lIt != getInputPorts().getPorts().end(); lIt++)
      lCRCErrorMetrics.push_back(&(*lIt)->getMetric(processor::InputPort::kMetricIdCRCErrors));

    swatch::core::ComplexMetric<uint32_t>& lTotalCRCs = registerComplexMetric<uint32_t>("totalCRCErrors", lCRCErrorMetrics.begin(), lCRCErrorMetrics.end(), &sumUpCRCErrors, &filterOutMaskedPorts);

    // System should go into error if total number of CRC errors in one processor goes above 50 ...
    setErrorCondition(lTotalCRCs, swatch::core::GreaterThanCondition<uint32_t>(50));

    // ...

Note that instead of adding each of the input metrics to an STL collection such as ``std::vector``, the input metrics can instead by specified using a ``swatch::core::MetricView`` which is created using a regex on the metrics' ID paths

.. code-block:: c++

  registerComplexMetric<uint32_t>("totalCRCErrors", swatch::core::MetricView(getInputPorts(), ".*" + processor::InputPort::kMetricIdCRCErrors), &sumUpCRCErrors, &filterOutMaskedPorts);


**Example 2:** Count the number of unmasked input ports that are not aligned (within one processor):

.. code-block:: c++

  bool filterOutMaskedPorts(const core::MonitorableObject& aObj)
  {
    // Ensures that only metrics within UN-masked input ports are included in the metric value calculation
    return !dynamic_cast<const processor::InputPort&>(aObj).isMasked();
  }

  const uint32_t* countUnalignedPorts(const std::vector<core::MetricSnapshot>& aSnapshots)
  {
    uint32_t lCount = 0;

    for (auto lIt=aSnapshots.begin(); lIt != aSnapshots.end(); lIt++) {
      if (lIt->isValueKnown()) {
        if (!lIt->getValue<bool>())
          lCount++;
      }
      // If the value of one of the ports' "isAligned" metric is unknown, then we do not know how many ports are not aligned
      else
        return NULL;
    }

    return new uint32_t(lResult);
  }

  MyCoolProcessor::MyCoolProcessor(const swatch::core::AbstractStub& aStub) :
    Processor(aStub),
    mDriver(new DummyProcDriver())
  {
    // First, must create all of the input ports
    // ...
    // ...

    // Now register the "unalignedCount" complex metric
    std::vector<core::AbstractMetric*> lIsAlignedMetrics;
    for (auto lIt=getInputPorts().getPorts().begin(); lIt != getInputPorts().getPorts().end(); lIt++)
      lIsAlignedMetrics.push_back(&(*lIt)->getMetric(processor::InputPort::kMetricIsAligned));

    swatch::core::ComplexMetric<uint32_t>& lUnalignedCount = registerComplexMetric<uint32_t>("unalignedCount", lIsAlignedMetrics.begin(), lIsAlignedMetrics.end(), &countUnalignedPorts, &filterOutMaskedPorts);

    // System should go into error if more than 2 input ports in one processor are not aligned ...
    setErrorCondition(lUnalignedCount, swatch::core::GreaterThanCondition<uint32_t>(2));

    // ...


Monitoring settings
^^^^^^^^^^^^^^^^^^^

You may not want certain monitorable objects / metrics within your processors and AMC13s to contribute to your system being in error/warning. For example, if you mask an input port, then you will not want its metrics to contribute to that processor's (or the system's) overall monitoring status.

For this reason, each monitorable object and metric has a *monitoring setting* that allows you to stop their error/warning status from contributing to the monitoring status of their parent nodes. Each monitorable object / metric can be:

* *Enabled* (default): That metric / monitorable object contributes to the status of parents
* *Non-critical:* That metric / monitorable object doesn’t contribute to the status of parents

  * Example usage: You've implemented an experimental feature in firmware that doesn’t affect running; you want to monitor if the corresponding metrics generates an error, but this feature is not critical for run, and so should not cause the processor/system to be in error.

* *Disabled:* That metric / monitorable object doesn’t contribute to status of parents; the values of *disabled* metrics are not updated.

  * Example usage: You've masked an input link; or, the firmware block associated with this monitorable object / metric is not implemented in the loaded bit file

The values of **monitoring settings are automatically updated as each processor/AMC13 moves through its FSM states** :

* Firstly, when a FSM is engaged/reset, all of the processor's/AMC13's decendent monitorable objects and metrics are all firstly reset to "enabled"
* When each processor/AMC13 enters a new FSM state, the FSM applies the monitoring settings that are specified for that state in the gatekeeper (i.e. the configuration XML file)

  * **N.B.** When processors and AMC13s move between states via normal transitions (i.e. excluding reset), the monitoring settings specified in the gatekeeper are applied, but the  monitoring settings of other monitorable objects and metrics are **not** reset to "enabled"

Monitoring setting values are specified in the XML configuration files as follows:

#. Add one or more ``state`` tags, as a child of the ``context`` tag; the ``id`` attribute specifies the ID of the state in which the monitoring settings will be applied.
#. Add one or more ``mon-obj`` / ``metric`` tags, as a child of the ``state`` tag; each ``mon-obj`` / ``metric`` tags must have two attributes:

  * ``id`` : The ID path of the metric / monitorable object whose setting you're specifying, relative to the processor's / AMC13's ID path; e.g. ``inputPorts.Rx00`` for input port ``Rx00`` within a processor.
  * ``status`` : The monitoring setting value; must be one of ``enabled`` , ``non-critical`` or ``disabled``

The same context look-up order is used for specifying monitoring settings as for command parameters (e.g. ``systemID.processorID``, ``systemID.processorRole``, ``systemID.processorID`` for processors).

For example, the following XML file snippet specifies some monitoring settings for disabled input ports that would be applied when processors enter the "run control" FSM's initial state, "Halted" (i.e. when the "run control" FSM is engaged or reset):

.. code-block:: xml

  <run-settings id="dummySys">
     <!-- INPUT PORT 0: disabled for all processors -->
     <context id="processors">
        <state id="Halted">
           <mon-obj id="inputPorts.Rx00" status="disabled" />
        </state>
     </context>

     <!-- INPUT PORTS 40-42 & 45: disabled only for processor with ID "procC" -->
     <context id="procC">
        <state id="Halted">
           <mon-obj id="inputPorts.Rx40" status="disabled" />
           <mon-obj id="inputPorts.Rx41" status="disabled" />
           <mon-obj id="inputPorts.Rx42" status="disabled" />
           <mon-obj id="inputPorts.Rx45" status="disabled" />
        </state>
     </context>
  </run-settings>



Accessing montitoring status and metrics' values
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The status of monitorable objects (i.e. error, warning, good, or unknown) and their metrics can be accessed through the following methods:

* Monitorable object: ``getStatus()`` returns a``swatch::core::MonitorableObjectSnapshot`` instance, that contains the monitorable objects status flag, and setting (i.e. enabled, non-critical, or disabled) 
* Metric: ``getSnapshot()`` returns a ``swatch::core::MetricSnapshot`` instance, that contains:

  * The metric's value
  * The time that it was last updated
  * Pointers to the associated error/warning condition objects, its status flag (good/warning/error/unknown)
  * Its monitoring setting (enabled/non-critical/disabled)

Both of these methods are **thread safe** - i.e. they will not cause any data corruption if called in a different thread than the thread in which the metrics' values are updated. However, if only using the APIs of the ``MonitorableObject`` and ``Metric`` classes, the values of a processor's (or DAQ-TTC manager's) metrics could be updated between multiple calls of the ``MonitorableObject::getStatus()`` and ``Metric::getSnapshot()`` methods. 


Consistent snapshots of multiple metrics
""""""""""""""""""""""""""""""""""""""""


In order to ensure that the metrics of a specific processor / DAQ-TTC manager instance are not updated during some section of code (e.g. while calling the ``getSnapshot()`` method on several metrics), you can create an instance of the ``MetricReadGuard`` class. The metrics of that processor / DAQ-TTC manager will not be updated until after the ``MetricReadGuard`` class has been destroyed.

.. note::
   You must contact the central online software team **before** deploying at Point 5 code that uses the ``MetricReadGuard``. This is not to discourage their use, but so that the central software team can check that their use is appropriate and will not lead to deadlocks.

For example, if you want to read the values of processor 'algo' metrics ``rateCounterA`` and ``rateCounterB``, you could ensure that you read their values from the same 'update cycle' as follows:

.. code-block:: C++

  swatch::core::MetricReadGuard guard(myProcessor);
  uint32_t rateCounterA = myProcessor.getAlgo().getMetric("rateCounterA").getSnapshot().getValue<uint32_t>();
  uint32_t rateCounterB = myProcessor.getAlgo().getMetric("rateCounterB").getSnapshot().getValue<uint32_t>();

  // myProcessor's metrics are at risk of being updated again in another thread once 'guard' is destroyed

.. warning::
   You must **NOT** create a ``MetricReadGuard`` instance for a processor/DAQ-TTC manager whilst another ``MetricReadGuard`` instance that was created by the same thread for the same object is still alive; doing that will cause a deadlock.


