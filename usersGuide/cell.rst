
.. _usersGuide-swatch-cell:

SWATCH in Trigger Supervisor cells
==================================

The purpose of the 'SWATCH cell' framework is to provide a common library that integrates a SWATCH system into a Trigger Supervisor (TS) cell, and includes implementations of common, subsystem-agnostic SWATCH-related utilities. There are two SWATCH-TS packages:

* ``swatchcell/framework`` : Contains common generic services that will form the common framework of the upgrade TS cells.
* ``swatchcell/example`` : A basic, minimal instantiation of what the SWATCH cell framework provides.

Only the RPM of the framework package needs to be installed on subsystem PCs (and indeed the yum installation instructions above do not install the swatchcell-example RPMs)


Step 1: Write the cell class
----------------------------

Each subsystem should implement a cell class that inherits from ``swatchcellframework::CellAbstract``, with a cell context that inherits from ``swatchcellframework::CellContext`` . This context class contains a pointer to the cell's SWATCH system object; initially, subsystems can just use the ``swatchcellframework::CellContext`` context class directly (there's no need to define your own derived class).

The subsystem-specific TS cell classes could be implemented in a package located in the ``cell`` subdirectory of the system's area - e.g. for subsystem ``systemA``, the first step is to create the following structure::

   cactusprojects/systemA/
      tscell/
         Makefile
         include/
             systemA/
                cell/
                   Cell.h
                   version.h
         src/
            common/
               Cell.cc
               version.cc

with ``Cell.h`` containing the following::

  #ifndef _systemA_cell_Cell_h_
  #define _systemA_cell_Cell_h_

  #include "swatchcell/framework/CellAbstract.h"

  namespace systemAcell
  {
    class Cell : public swatchcellframework::CellAbstract
    {
    public:

      XDAQ_INSTANTIATOR();

      Cell(xdaq::ApplicationStub * s);

      ~Cell();

      void init();
  	
    private:
      Cell( const Cell& );
    };
  }
  #endif


and ``Cell.cc`` containing the following::


  #include "systemA/cell/Cell.h"

  XDAQ_INSTANTIATOR_IMPL(systemAcell::Cell)

  systemAcell::Cell::Cell(xdaq::ApplicationStub * s) :
    swatchcellframework::CellAbstract(s) 
  {       
  }

  systemAcell::Cell::~Cell() 
  { 
  }

  void 
  systemAcell::Cell::init()
  {


Then, after implementing the ``Makefile`` and ``version.h`` / ``.cc`` for this package, make sure that the package compiles before moving onto the next step.


Step 2: Add the generic SWATCH TS panels
----------------------------------------

During the development of SWATCH, a few common SWATCH-TS panels have been implemented so far.

* ``InitSwatchJsonFile`` : Loads SWATCH system from JSON file description (see "System description" section later on for format of system description JSON files)

  * Click on ``InitSwatchJsonFile`` under ``Trigger Supervisor`` / ``Commands`` / ``SWATCH`` in the upper-left menu
  * Enter the local pathname of the JSON file describing your system in the input box (can include environment variables), and click "Execute"
  * Wait for the "Reply" box to report success / error; if sucessful, you'll see output similar to the following: ``Swatch system 'exampleSys_p5' built from file ${swatchexample_ROOT}/test/p5example.json``

* ``SWATCH monitoring`` : A few simple generic monitoring panels

  * Click on ``SWATCH monitoring`` under ``Trigger Supervisor`` / ``Control panel`` in the upper-left menu
  * One of three subpanels can be selected by the radio box buttons:

    * ``System summary``

      * Click the "Refresh" button to query your hardware, and see new monitoring data.
      * The first table - "Processors" - lists the processors in the system, and shows the overall status - good, warning, error, unknown (problem retrieving monitoring data) - of each processor and its four main components (TTC, optical links, readout, and algo block)
      * The second table - "AMC13s" - lists the DaqTTCManagers in the system, shows the overall status of each one, and the values of the DaqTTCManager metrics
      * The third table - "Internal links" - lists the system's internal links, and shows the overall status of the input port and output port in each link.

    * ``Processors``

      * Again, click the "Refresh" button to query your hardware, and see new monitoring data.
      * The first table - "Summary" - lists the processors in the system, and shows the overall status of each processor and its four main componenents, as well as the values of the common processor metrics
      * The 2nd table - "TTC details" - lists the processors in the system, shows the overall status of each processor's TTC interface, and the values of the common TTC metrics for each processor
      * The 3rd table - "Input port details" - contains the corresponding information for all input optical ports
      * The 4th table - "Output port details" - contains the corresponding information for all output optical ports

    * ``Metric view``

      * This panel allows you to view the value, status (good / bad / error / no limit), error condition, warning condition, and update time, for all metrics whose dot-delimited ID paths match some regular expression (regex)
      * E.g. if you have a system containing AMC13s whose ID strings contain "AMC13", and want to look at all metrics of the AMC13s and the processor TTC blocks, you would use a regex ``.*(AMC13|ttc).*``

  * ``SWATCH commands`` : Run commands on a processor or DaqTTCManager

    * Click on ``SWATCH commands`` under ``Trigger Supervisor`` / ``Control panel`` in the upper-left menu
    * Select the object type (i.e. C++ class name) in the first drop-down menu, then the device ID in the 2nd menu, and the command in the third menu.
    * If this command requires any parameters to run, then they you can set their values by entering the corresponding string in the following input boxes
    * Then click "Run command"
    * The black box below will update every 5 to 10 seconds with the progress of the command


These panels will still evolve in future release, but already in their current state, they can be used to start exercising subsystem-specific classes, commands, and metrics, without needing to write your own panels straight away.

These generic SWATCH TS panels can be added to the subsystem's cell by calling the ``addGenericSwatchComponents()`` method within your subsystem cell's ``init()`` method - i.e.::

  void 
  Cell::init()
  {
    addGenericSwatchComponents();
    getContext()->getCell()->getApplicationDescriptor()->setAttribute("appPath", "swatchcell/framework"); 
  }

Step 3: Try running the cell in a basic, standalone mode
--------------------------------------------------------

You can run your skeleton SWATCH-based TS cell in a basic, standalone mode by running the script ``/opt/xdaq/bin/swatchcell/runSubsystemCell.sh``, with the following environment variables set:

* ``SUBSYSTEM_ID`` : The ID of your subsystem (e.g. calol1 , calol2 , ugt , ...); must not contain spaces.
* ``SUBSYSTEM_CELL_CLASS`` : Fully-qualified name of your Cell class (e.g. ``calol2::cell::Cell`` )
* ``SUBSYSTEM_CELL_LIB_PATH`` : Path to the library containing your Cell class (multiple libraries can be specified using a semi-colon as delimeter ; )
* ``SWATCH_DEFAULT_INIT_FILE`` : path for system description JSON file
* ``SWATCH_DEFAULT_GATEKEEPER_XML`` : path for XML configuration file
* ``SWATCH_DEFAULT_GATEKEEPER_KEY`` : run key for XML configuration file 

*N.B.* When running in this basic standalone mode, the TS cell does not have access to the various services that will be needed eventually for Point 5 operations (e.g. TStore for database access); but, running in this standalone mode may be useful during initial, rapid development of your subsystem's SWATCH plugins. By default, on startup it will just contain an empty SWATCH system, but your own SWATCH system can be loaded by providing a JSON file description of your system (see "System description" section later on for format of system description JSON files), after having created your specialisations of the swatch processor and processor component interface classes (explained in the next section).

Of course, in time more files containing subsystem-specific classes, functions, etc could be added to this package in order to create subsystem-specific panels

*N.B.2* The cell's SWATCH system object is stored within the cell's context object, and accessible through the ``getSystem()`` method of the ``swatchcellframework::CellContext`` class. Whenever the system is accessed in a subsystem-specific panel, you should lock access to the system class and its processor/DaqTTCManager/crate/link objects by creating an instance of the the ``SystemGuard_t`` class.



