

Introduction
============

SWATCH is a C++ library designed to provide a generic interface for controlling and monitoring the 2016 Level-1 trigger upgrade hardware.

The generic interface is based on the following classes:

* Processor (``swatch::processor::Processor``): Represents the uTCA cards that process trigger primitive data
* DaqTTCManager (``swatch::dtm::DaqTTCManager``): Represents the connection to the central TCDS and DAQ networks (i.e. the AMC13)
* System (``swatch::system::System``): Represents a trigger subsystem composed of one or more processors and DaqTTCManagers (e.g. calo layer-1, calo layer-2, uGT, etc)

Each processor contains the following interfaces:

* TTC interface (``swatch::processor::TTCInterface``)
* Readout interface (``swatch::processor::ReadoutInterface``)
* Input optical port (``swatch::processor::InputPort``)
* Output optical port (``swatch::processor::OutputPort``)
* Algorithm interface (``swatch::processor::AlgoInterface``)

These generic interface classes contain a built-in common monitoring and control framework.  There are three concepts of actions for controlling hardware in SWATCH:

* Commands: One shot action - e.g. reset board, configure links
* Command sequences: Multiple commands executed in succession
* Operations: FSMs (Finite State Machines); each transition is typically a single command or command sequence

The monitoring framework is based around two main types of classes:

* Metrics: Represent monitoring data that is retrieved from the hardware, with associated error/warning conditions that determine if any given metric value is "Good", indicates an error, or should be flagged up as a warning
* Monitorable objects (processors, processor component interfaces, systems): An object that can contain metrics, and other monitorable objects

Subsystem developers should implement classes that inherit from the generic interface classes mentioned above. Subsystem-specific monitorable quantities, commands and finite-state machines (e.g. for run control) are registered in the constructors of the derived classes. The monitorable data retrieved from hardware is hooked into the framework by implementing a specific method (details explained in the following sections).

The SWATCH library itself has no dependency on the Trigger Supervisor, but a common SWATCH TS cell framework is also being developed. Currently this framework includes some generic monitoring and control panels; in time, this will also include classes and functions implementing various other common utilities. There are two SWATCH-TS packages:

* ``swatchcell/framework`` : Contains the generic services that will form the common framework of the upgrade TS cells.
* ``swatchcell/example`` : A basic, minimal instantiation of what the SWATCH cell framework provides.

Each subsystem's cell class should inherit from ``swatchcellframework::CellAbstract``.

**N.B:** The sections below give guidance on how to create SWATCH plugins for your subsystem; whilst reading those sections it might be useful to have the SWATCH doxygen open in another tab: http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/

Common SWATCH plugins
---------------------

During the development of SWATCH, we've created common SWATCH plugins to control and monitor:

* AMC13s
* Processors that use the generic MP7 infrastructure firmware (e.g. for their optical I/O, TTC and/or readout components)
