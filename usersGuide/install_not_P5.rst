Installation outside P5/904
===========================

The latest version of SWATCH is v1.3.0; this can be installed via YUM for 64-bit CERN CentOS7.6 machines by following the instructions below.

A docker image containing a SWATCH installation like described here is also provided for convenience under the tag 
``gitlab-registry.cern.ch/cms-cactus/core/swatch/xdaq15-swatch13:tag-v1.3.0``

#. XDAQ 15

   * **Remove any previous versions** of XDAQ::

        sudo yum -y groupremove cmsos_core cmsos_core_debuginfo cmsos_worksuite cmsos_worksuite_debuginfo

   * Download :download:`xdaq.repo <yum-repo-files/xdaq.repo>`, to ``/etc/yum.repos.d/xdaq.repo``
   
   * Install XDAQ::

      sudo yum clean all
      sudo yum -y groupinstall cmsos_core cmsos_core_debuginfo cmsos_worksuite cmsos_worksuite_debuginfo --exclude cmsos-worksuite-dipbridge --exclude cmsos-worksuite-amc13controller

#. Trigger supervisor 5.2

   * **Remove any previous versions** of the trigger supervisor RPMs::

        sudo yum groupremove triggersupervisor
        sudo yum remove "cactuscore-ts*"

   * Download :download:`cactus-ts.repo <yum-repo-files/cactus-ts.repo>`, to ``/etc/yum.repos.d/cactus-ts.repo``
   
   * Install triggersupervisor::

      sudo yum clean all
      sudo yum groupinstall triggersupervisor

#. uHAL version 2.7

   * **Remove any previous versions** of the uHAL RPMs::

      sudo yum groupremove uhal
      sudo yum remove "cactuscore-*-pugixml"
      sudo yum remove "cactuscore-uhal-*"

   * Download :download:`ipbus-sw.repo <yum-repo-files/ipbus-sw.repo>`, to ``/etc/yum.repos.d/ipbus-sw.repo``
   
   * Install uHAL::

      sudo yum clean all
      sudo yum groupinstall uhal

#. SWATCH

   * **Remove any previous versions** of SWATCH::

      sudo yum remove "cactuscore-swatch*"
      sudo rm /etc/yum.repos.d/cactus-swatch.repo
      sudo yum clean all

   * Download :download:`cactus-swatch.repo <yum-repo-files/cactus-swatch.repo>`, to ``/etc/yum.repos.d/cactus-swatch.repo``

   * Install SWATCH::

      sudo yum clean all
      sudo yum groupinstall swatch swatchcell

#. If you plan to compile SWATCH-dependant code on this machine (as opposed to simply running executables), then you'll need to also install::

      sudo yum -y install boost-devel pugixml-devel jsoncpp-devel


Common SWATCH plugins for the AMC13
-----------------------------------

In order to install on non-centrally-managed machines outside of Point 5, please follow these instructions:

#. AMC13 software version 1.2.15

   * **Remove any previous versions** of AMC13::
   
      sudo yum remove "cactusboards-amc13*"

   * Download :download:`cactus-amc13.repo <yum-repo-files/cactus-amc13.repo>`, to ``/etc/yum.repos.d/cactus-amc13.repo``
   
   * install AMC13::

      sudo yum clean all
      sudo yum install cactusboards-amc13-amc13-1.2.15 cactusboards-amc13-tools-1.2.15 cactusboards-amc13-python-1.2.15

#. Install the SWATCH plugin::

    sudo yum install cactuscore-swatch-amc13


Common SWATCH plugins for the MP7
---------------------------------

In order to install on non-centrally-managed machines outside of Point 5, please follow these instructions:

#. MP7 software version 2.6.0

   * **Remove any previous versions** of MP7 (e.g. 2.0.X versions)::
   
      sudo yum groupremove mp7

   * Download :download:`cactus-mp7 <yum-repo-files/cactus-mp7.repo>`, to ``/etc/yum.repos.d/cactus-mp7.repo``
   
   * Install the MP7 plugin::

      sudo yum clean all
      sudo yum groupinstall mp7

#. Install the SWATCH plugin::

    sudo yum install cactuscore-swatch-mp7
