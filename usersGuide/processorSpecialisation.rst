
Subsystem specialisation: The processor and its interfaces
==========================================================

The first step in creating subsystem-specific SWATCH plugins is to create a custom SWATCH processor. As mentioned earlier, the common aspects of trigger processors (CTP7s, MP7s, and MTF7s) are represented in SWATCH by the generic, abstract class ``swatch::processor::Processor``, whose interface in turn exposes other abstract interface classes that represent different aspects of the processor:

* TTC interface (``swatch::processor::TTCInterface``)
* Readout interface (``swatch::processor::ReadoutInterface``)
* Input optical port (``swatch::processor::InputPort``)
* Output optical port (``swatch::processor::OutputPort``)
* Algorithm interface (``swatch::processor::AlgoInterface``)


Step 1: Write a custom processor class
--------------------------------------

In order to create a custom SWATCH processor, firstly one must define a class that inherits from ``swatch::processor::Processor`` as follows:


.. code-block:: c++

  #include "swatch/processor/Processor.hpp"

  #include "path/to/MyHardwareDriver.hpp"

  class MyProcessor : public swatch::processor::Processor {
  public:
    MyProcessor(const swatch::core::AbstractStub& aStub);

    ~MyProcessor();

    void retrieveMetricValues();

  private:
    MyHardwareDriver* driver_;
  };


*N.B:*

* Here, the member data ``driver_`` is a pointer to the processor's hardware-specific driver class, in case one already exists outside of SWATCH.
* The ``retrieveMetricValues()`` method must be implemented in all subsystem processor classes - it's a pure virtual method declared in the ``swatch::core::MonitorableObject`` class. The implementation of this method reads from hardware the values of various monitoring data in a subsystem-specific way, and stores these values allowing these metrics to be accessible from the SWATCH framework. The contents of this method will be explained in more detail later on in the "Monitoring" section; until you get to that section, its implementation can be left blank.


Step 2: Register customised processor class
-------------------------------------------

In order for this class to be able to be created by the SWATCH framework, it must be registered with the SWATCH class factories, by using the ``SWATCH_REGISTER_CLASS`` macro in the ``MyProcessor.cpp`` file - e.g:


.. code-block:: c++

  #include "path/to/MyProcessor.hpp"

  #include "swatch/core/Factory.hpp"

  SWATCH_REGISTER_CLASS(mynamespace::MyProcessor);


With this line included in the cpp file, the subystem-specifc processor class can be created by the SWATCH framework by setting the processor's "CREATOR" value to the class name (including namespaces) in the JSON file / database entries that describe the system. (There will be more detail in later sections on these JSON files / database entries that are used to describe systems.)


Step 3: Write custom processor component classes
------------------------------------------------

With the custom processor class defined, custom interface classes representing the different components of processors must also now be created; specifically, this means implementing one class that inherits from each of the following abstract classes:

* ``swatch::processor::TTCInterface`` (represents processor's TTC block)
* ``swatch::processor::ReadoutInterface`` (represents processor's readout block)
* ``swatch::processor::InputPort`` (represents an input optical port on the processor)
* ``swatch::processor::OutputPort`` (represents an output optical port on the processor)
* ``swatch::processor::AlgoInterface`` (represents the processor's algo block)

E.g. For the subsystem-specifc TTC interface class the header file would look something like:


.. code-block:: c++

  #include "swatch/processor/TTCInterface.hpp"

  #include "path/to/MyHardwareDriver.hpp"

  class MyTTCInterface : public swatch::processor::TTCInterface {
  public:
    MyTTCInterface(MyHardwareDriver& driver);
    ~MyTTCInterface();

    void retrieveMetricValues();

  private:
    MyHardwareDriver& driver_;
  };


and the cpp file could look something like:


.. code-block:: c++

  #include "path/to/MyTTCInterface.hpp"

  MyTTCInterface::MyTTCInterface(MyHardwareDriver& driver) :
    driver_(driver)
  {}

  MyTTCInterface::~MyTTCInterface() {}


  void MyTTCInterface::retrieveMetricValues() {
    // Read bunch, orbit & event counters from hardware
    // Note, of course, the following three lines could be very different for different subystems,
    // depending on how they read these values from their hardware, and the API of their driver classes.
    uint32_t bunchCounterValue = driver_.readBunchCounter();
    uint32_t orbitCounterValue = driver_.readOrbitCounter();
    uint32_t eventCounterValue = driver_.readEventCounter();

    // Plug these values into the SWATCH metric framework
    setMetricValue<>(metricBunchCounter_, bunchCounterValue);
    setMetricValue<>(metricOrbitCounter_, orbitCounterValue);
    setMetricValue<>(metricEventCounter_, eventCounterValue);

    // N.B: This method should also set the values of 5 other common metrics that are
    // defined in the swatch::processor::TTCInterface class; they have been missed out here to keep the example brief
  }



Step 4: Register custom processor component classes
---------------------------------------------------

Once the subsystem-specific TTC, readout, algo, input port and output port classes exist, they need to be registered in the constructor of the subsystem-specific processor class, through the ``registerInteface`` methods that are inherited from ``swatch::processor::Processor``; e.g. for the ``MyProcessor`` constructor:


.. code-block:: c++

  #include "path/to/MyProcessor.hpp"
  #include "path/to/MyAlgoInterface.hpp"
  #include "path/to/MyInputPort.hpp"
  #include "path/to/MyOutputPort.hpp"
  #include "path/to/MyReadoutInterface.hpp"
  #include "path/to/MyTTCInterface.hpp"

  #include "path/to/MyHardwareDriver.hpp"

  #include "swatch/processor/PortCollection.hpp"

  // ...

  MyProcessor::MyProcessor(const swatch::core::AbstractStub& aStub) :
    swatch::processor::Processor(aStub)
    driver_(NULL)
  {
    // Access the swatch::processor::ProcessorStub object - a struct, created from the JSON file / database entry describing this processor...
    // .. the processor stub contains this processor's physical location; the board's URI and address table name/location; lists of stubs describing the processor's input and output ports
    const swatch::processor::ProcessorStub& stub = getStub();

    driver_ = new MyHardwareDriver(stub.addressTable, stub.uri);

    // Build subcomponents
    registerInterface( new MyTTCInterface( *driver_ ) );
    registerInterface( new MyReadoutInterface(*driver_) );
    registerInterface( new swatch::processor::PortCollection() );

    // The swatch::processor::PortCollection class is used to aggregate together all of the processor's optical ports ...
    // ... and hence, the input/output ports are registered by adding them to the link interface (accessed through the getPortCollection method)
    for(std::vector<swatch::processor::ProcessorPortStub>::const_iterator it = stub.rxPorts.begin(); it != stub.rxPorts.end(); it++)
      getPortCollection().addInput(new MyRxPort(it->id, it->number, *this));
    for(std::vector<swatch::processor::ProcessorPortStub>::const_iterator it = stub.txPorts.begin(); it != stub.txPorts.end(); it++)
      getPortCollection().addOutput(new MyTxPort(it->id, it->number, *this));
  }



*N.B.* For more information on what information the ``swatch::processor::ProcessorStub`` object contains, see its doxygen page: http://cactus.web.cern.ch/cactus/release/swatch/1.2/api/html_swatch/dd/d98/classswatch_1_1processor_1_1_processor_stub.html

Now that you've defined some subsystem-specific classes, the next steps are to hook into the SWATCH control and monitoring frameworks, which are explained in the next two sections ...
