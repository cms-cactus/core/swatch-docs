.. testdrive documentation master file, created by
   sphinx-quickstart on Sat Jan 23 17:27:43 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Release notes
=============

Welcome to the SWATCH release notes!

Contents:

.. toctree::
   :maxdepth: 2

   v1_3_0
   v1_2_4
   v1_2_3
   v1_2_2
   v1_2_1
   v1_2_0
   v1_1_2
   v1_1_1
   v1_1_0
   v1_0_1
   v1_0_0
   v1_0_0_alpha2
   v1_0_0_alpha1
   v0_12_0
   v0_11_2
   v0_11_0
   v0_10_2
   v0_10_1
   v0_10_0
   v0_9_2
   v0_9_1
   v0_9_0
   v0_8_2
   v0_8_1
   v0_8_0
   v0_7_1
   v0_7_0
   v0_6_0
   v0_5_2
   v0_5_1
   v0_5_0
   v0_4_1
   v0_4_0
   v0_3_1
   v0_3_0
   v0_2_0
   v0_1_0
