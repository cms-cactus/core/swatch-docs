Version 1.3.0
==============

YUM repositories created on 25/08/2020 ; deployed at Point 5 on TDB

**Main new features:**

* Source code updated to compile on CERN CentOS7.6, with GCC v8.3, XDAQ v15 and Trigger Supervisor 5.2 (first dojo-free release of TS)


Migration guide: v1.2.4 to v1.3.0
-----------------------------------

**Installation:**

 * **N.B.** SWATCH v1.3.0 requires the following prerequisites to be installed:

   * XDAQ: v15
   * uHAL: v2.7.7
   * Trigger supervisor: v5.2.0
   * AMC13 software (for ``swatch-amc13`` packages): v1.2.15
   * MP7 software (for ``swatch-mp7`` packages): v2.6.0

 * Centrally managed machines at Point 5 will be upgraded to version 1.3.0 when they are migrated to CentOS7.6 (exact date/time TBD)
 * For centrally managed machines in building 904, please open a ticket at https://its.cern.ch/jira/projects/CMSONS/issues/ to request the new version
 * For other machines ( *NOT* at Point 5 or in 904), the older packages must be first removed, following the commands in the installation section of the user's guide

**Source code:**

* SWATCH is now compiled with CentOS Devtools 8 (i.e. GCC8). Take care to use the same GCC version.
  You can do this by running ``yum install -y centos-release-scl devtoolset-8-gcc devtoolset-8-gcc-c++`` and ``/usr/bin/scl enable devtoolset-8``

* In XDAQ15, the exact way in which certain variables are used in Makefiles has changed, and so you may find that you need to update your makefiles and/or your repository's directory structure in order to successfully build and package subsystem software. In particular:

  .. warning:: The XDAQ makefiles impose strict requirements on the values of certain variables and the names of some macros and variables in the ``version.{h,cc}`` files. When these requirements are not met, the error messages may not directly report the cause of the problem. If you encounter build problems we suggest that you carefully compare your subsystem's makefiles, directory structure and source code with that of the calorimeter layer 2 and example repositories noted below.

  * The XDAQ makefile variable & rules files have been moved from ``/opt/xdaq/config`` to ``/opt/xdaq/build``
  * XDAQ makefiles no longer include the ``Project`` variable in the path where they expect your packages to be located. The following repository structure has been found to work with the new XDAQ release:

    .. code-block:: none

      mySubsystemRepo/
        mySubsystem/
          packageA/
            Makefile
            include
            src
          packageB/
            Makefile
            include
            src

    With this repository structure, lines that automatically create symlinks in the repository's root ``Makefile`` (e.g. ``$(shell test ! -d $(Project) && mkdir -p `dirname $(Project)` && ln -s `pwd` $(Project))``) can be removed, and the project/package variables in ``packageA/Makefile`` should have the following values:

    .. code-block:: none

      Project=cactusprojects-mySubsystem
      Package=mySubsystem/packageA
      PackageName=packageA

    Additionally, XDAQ will require that the version macros in your ``version.h`` files (e.g. ``mySubsystem/packageA/version.h``) have the following format: ``MYSUBSYSTEM_PACKAGEA_VERSION_{MAJOR,MINOR,PATCH}``

    .. note:: A complete set of subsystem changes for XDAQ15 migration can be found in the calorimeter layer 2 repository: https://gitlab.cern.ch/cms-cactus/projects/calol2/-/merge_requests/3/diffs

    .. note:: An example dummy project demonstrating an empty SWATCH 1.3.0 project can be found here: https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/example-swatch-cell/-/tree/swatch-1.3.0


**System description file:**  No changes required.


**XML configuration files** (that store command parameters, monitoring settings and masks): No changes required.
