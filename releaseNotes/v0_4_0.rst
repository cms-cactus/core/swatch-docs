
Version 0.4.0
=============

YUM repositories created on 01/02/2016 ; deployed at Point 5 on 03/02/2016

**Main new features:**

* **XML configuration files:**

  * Look up tables: Ability to specify vectors (``xdata::Vector<xdata::UnsignedInteger>``, ``xdata::Vector<xdata::Integer>``, etc) in XML configuration files
  * Configuration data is split over multiple XML files

* **Standard interface for maskable objects** (e.g. input ports)

  * C++ interface, and ability to specify in the XML configuration file if any specific maskable object is masked

* **Monitoring settings:**

  * Used to stop parts of the swatch object tree from contributing to the overall monitoring status of the processor/AMC13/system.
  * Can be specified in XML configuration files (linked to state machine states)

* Mechanism to exclude specific processors and AMC13s from system-level actions at runtime

  * Excluded processors/AMC13s can be specified in the XML 

* Mechanism for ensuring thread safe access to driver when updating monitoring data and running actions (commands, transitions, etc) in different threads


**Bugfixes:**

* Setup panel no longer segfaults if a processor doesn't contain any input/output ports


**Backward incompatible changes:**

* Moved ``swatch::processor::Link`` and ``swatch::processor::LinkStub`` into the ``swatch::system`` namespace, i.e. new class names are ``swatch::system::Link`` and ``swatch::system::LinkStub``
* Separated ``swatch::processor::PortCollection`` into two objects:

  * ``swatch::processor::InputPortCollection`` that contains the input ports
  * ``swatch::processor::OutputPortCollection`` that contains the output ports

* XML configuration files: 

  * ``run`` tag renamed as ``key``, ``key`` attribute renamed as ``id``
  * ``table`` tag renamed as ``context``
  * ``entry`` tag renamed as ``param``
  * Configuration data must now be stored in multiple files, with a single top-level file referencing one or more configuration module files


**Minor features:**

* log4cplus is now used as the logging library, with a separate logger for each processor/AMC13

  * There is also a logger panel that displays the log messages from the main loggers (adapted from the legacy RPC's log widget)



Migration guide: v0.3.x to v0.4.0
---------------------------------

*Note:* Subsystem software will have to be recompiled after version 0.4 is installed.

**Installation:**

* *N.B.* SWATCH v0.4 requires version 3.0 of the trigger supervisor libraries to be installed.
* Centrally managed machines at Point 5 have been upgraded to version 0.4.0 on 03/02/2016
* For centrally managed machines in building 904, please open a ticket at https://its.cern.ch/jira/projects/CMSONS/ to request the new version
* For other machines ( **NOT** at Point 5 or in 904), the version 0.1 packages must be first removed, following the commands in the installation section of the user's guide


**Source code** (i.e. to avoid compilation errors):

* The protected ``ActionableObject`` action-registration methods, used to register commands and command sequences in processors/DaqTTCManagers, have been renamed:

  * Registering commands:  ``registerFunctionoid<T>`` becomes ``registerCommand<T>``
  * Registering command sequence:  ``registerCommandSequence`` becomes ``registerSequence``

* The signature of the protected ``swatch::core::Command`` constructor has been updated

  * from ``template <typename T> Command(const std::string& aId, const T& aDefaultResult)``
  * to ``template<typename T> Command( const std::string& aId , ActionableObject& aResource, const T& aDefault );`` where aResource, 
    this means the the constructors of classing inheriting from ``swatch::core::Command`` must change from:

    .. code-block:: c++

      MyCoolCommand::MyCoolCommand(const std::string& aId) :
        swatch::core::Command(aId, xdata::Integer(42) /* default result */)

    to:

    .. code-block:: c++

      MyCoolCommand::MyCoolCommand(const std::string& aId, swatch::core::ActionableObject& aActionable) :
        swatch::core::Command(aId, aActionable, xdata::Integer(42) /* default result */)


* The following protected methods of ``swatch::core::Command`` have been renamed/refactored

  * The ``getParent`` method template has been renamed as ``getActionable``, and now returns a reference rather than a pointer

    * i.e. the method  ``template <typename T> T* getParent<T>()`` has changed to: ``template <typename T> T& getActionable<T>()``

* Update any use of ``swatch/processor/LinkStub.hpp`` header file to ``swatch/system/LinkStub.hpp``
* Update any use of ``swatch/processor/Link.hpp`` header file to ``swatch/system/Link.hpp``
* The input ports and output ports must now be registered in different collection classes - ``InputPortCollection`` and ``OutputPortCollection`` respectively. For example, if your ports were registered as follows with SWATCH v0.3.x:

  .. code-block:: c++

    // Register port collection
    registerInterface( new swpro::PortCollection() );

    // Add input and output ports
    for(auto it = stub.rxPorts.begin(); it != stub.rxPorts.end(); it++)
      getPorts().addInput(new MyRxPort(it->id, it->number, *this));
    for(auto it = stub.txPorts.begin(); it != stub.txPorts.end(); it++)
      getPorts().addOutput(new MyTxPort(it->id, it->number, *this));


  then in SWATCH v0.4.0 these lines of code should be changed to:

  .. code-block:: c++

    // Register port collections
    registerInterface( new swatch::processor::InputPortCollection() );
    registerInterface( new swatch::processor::OutputPortCollection() );

    // Add input and output ports
    for(auto it = stub.rxPorts.begin(); it != stub.rxPorts.end(); it++)
      getInputPorts().addPort(new MyRxPort(it->id, it->number, *this));
    for(auto it = stub.txPorts.begin(); it != stub.txPorts.end(); it++)
      getOutputPorts().addPort(new MyTxPort(it->id, it->number, *this));


* Update any use of ``swatchcell/framework/SwatchCellAbstract.h`` header file to ``swatchcell/framework/CellAbstract.hpp``
* The class ``swatchcell::framework::SwatchCellAbstract`` has been renamed ``CellAbstract``

  * ``swatchcellframework::SwatchCellAbstract`` becomes ``swatchcellframework::CellAbstract``

* Add the following line to the constructor of your ``Cell`` class:

  .. code-block:: c++

    getContext()->getCell()->getApplicationDescriptor()->setAttribute("appPath", "swatchcell/framework");



**System description JSON file:** No changes required.


**XML configuration files** (that store command parameters, monitoring settings and masks):

* The name of the ``run`` tag has changed to ``key``, and the ``key`` attribute has been renamed as ``id``

  * i.e. change ``<run key=`` to ``<key id=``

* The ``table`` tag has been renamed as ``context``
* The ``entry`` tag (used to specify the values of parameters for commands) has been renamed as ``param``
* The data must now be stored in multiple files, with a single top-level file referencing one or more configuration module files. Each configuration module has a ``module`` tag as its root element, whose children are ``context`` and ``disable`` tags. For detailed examples, please see the user's guide

