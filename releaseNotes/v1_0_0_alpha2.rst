Version 1.0.0, alpha2
=====================

YUM repositories created on 22/05/2017 ; final v1.0.0 release will be deployed at Point 5 around the end of May.

**Main new features:**

* Run control operation now checks that IDs of all contexts in configuration data match items in the hardware description

  * Run control operation will raise an error if the ID of any context ID does not match the ID/role of any items from the hardware description

* Improved readability of error/warning messages issued by run control operation (superfluous details removed, much shorter if multiple commands fail with same error)
* Updated SWATCH exception classes to inherit from ``xcept::Exception`` class, and use related XDAQ macros (e.g. ``XCEPT_RAISE``)

**Minor improvements**

* ``ReadWriteXParameterSet``: Add non-const getter method
* ``Object`` class: Add method to access dependants by full ID path
* Moved 'periodic worker thread' implementation out of ``swatchcellframework::MonitoringThread`` class into generic thread class, that can be re-used for other purposes (e.g. for threads that publish rates in uGT) 
* Code cleanup:

  * Merged ``swatchcellframework::RunControlBase`` and ``swatchcellframework::RunControl`` classes

**Bugfixes**

* Fix problem with special characters (e.g. ``+``) in command and command sequence panels

**Backward incompatible changes**

* Moved ``State`` enum from ``swatch::core::ActionSnapshot`` class to ``swatch::core::Functionoid`` class
* Renamed the metric classes to be more consistent: ``Metric`` becomes ``SimpleMetric`` and ``MetricBase`` becomes ``Metric``
* Moved several action-related classes (e.g. ``Command``, ``Transition``, ``StateMachine``) from the ``cactus_swatch_core`` library to a new ``cactus_swatch_action`` library

  * Corresponding classes moved from ``swatch::core`` namespace to ``swatch::action``, and header files moved from ``swatch/core`` directory to ``swatch/action``

* Renamed the command's "detailed info" methods and member data to "execution details"
* Moved AMC13 command classes into the ``swatch::amc13::cmds`` namespace, split up code into more headers and cpp files, now located under ``swatch/amc13/cmds``
* Moved MP7 command classes into the ``swatch::mp7::cmds`` namespace, split up code into more headers and cpp files, now located under ``swatch/mp7/cmds``


Migration guide: v0.12.0 to v1.0.0.alpha2
-----------------------------------------

**Installation:**

 * **N.B.** SWATCH v1.0.0 requires the following prerequisites to be installed:

   * XDAQ: v14
   * uHAL: v2.5.x
   * Trigger supervisor: v4.0.0
   * AMC13 software (for ``swatch-amc13`` packages): v1.2.5
   * MP7 software (for ``swatch-mp7`` packages): v2.2.3


**Source code:**

* Core SWATCH classes:

  * Moved several action-related classes (e.g. ``Command``, ``Transition``, ``StateMachine``) from the ``cactus_swatch_core`` library to a new ``cactus_swatch_action`` library

    * Classes moved from ``swatch::core`` namespace to ``swatch::action``; header files moved from ``swatch/core`` directory to ``swatch/action``
    * **N.B.** You can use the ``updateFilesAfterSwatchCoreActionSplit.sh`` script to automatically update your source code to use the new ``swatch/action`` header files, and ``swatch::action`` namespace for the affected classes. Example usage::

      /opt/cactus/bin/swatch/updateFilesAfterSwatchCoreActionSplit.sh `find . -iname "*.h" -or -iname "*.cc"`

    * You will also need to update any makefiles of libraries that link against ``cactus_swatch_core`` to also link against ``cactus_swatch_action``
    * Affected classes (excl. exceptions):

      * User-facing: ``ActionableObject``, ``Command``, ``CommandSnapshot``, ``CommandSequence``, ``Functionoid``, ``GateKeeper``, ``StateMachine``, ``SystemStateMachine``, ``SystemTransition``, ``SystemTransitionSnapshot``, ``Transition``, ``TransitionSnapshot``
      * Internal: ``ActionableStatus``, ``ActionableSystem``, ``BusyGuard``, ``CommandVec``, ``CommandSequenceSnapshot``, ``CommandVecSnapshot``, ``GateKeeperView``, ``MaskableObject``, ``MonitoringSetting``, ``ObjectFunctionoid``, ``SystemBusyGuard``, ``SystemFunctionoid``, ``SystemStateMachine``, ``ThreadPool``

  * The definition of the ``State`` enum that is returned by the ``Command::code`` method has moved from the ``ActionSnapshot`` class to the ``Functionoid`` class (from which the ``Command`` class inherits)

    * You may need to update references to this enum, or its values, in definitions of subsystem-specific command classes - e.g. to:

      .. code-block:: c++

        class MyCommandClass : public swatch::action::Command {
          // ...

          State code(const swatch::core::XParameterSet& aParamSet);
          
        }

        // ...

        MyCommandClass::State MyCommandClass::code(const swatch::core::XParameterSet& aParamSet)
        {
           // ...
           return State::kError;
        }

  * The metric classes have been renamed as follows: ``Metric`` becomes ``SimpleMetric`` and ``MetricBase`` becomes ``Metric``

    * You will have to replace any usage of ``swatch::core::Metric`` in your code to ``swatch::core::SimpleMetric``
    * Similarly, any usage of the ``swatch/core/Metric.hpp`` header file should be updated to ``swatch/core/SimpleMetric.hpp``
    * The name of the ``MonitorableObject`` method for registering simple metrics - ``registerMetric`` - remains the same

  * The command's "detailed info" methods and member data have been renamed to "execution details", specifically:

    * The ``Command`` ``addDetailedInfo`` method becomes ``addExecutionDetails``
    * The ``Command`` ``setDetailedInfo`` method becomes ``setExecutionDetails``
    * The ``CommandSnapshot`` ``getDetailedInfo`` method becomes ``getExecutionDetails``

* Common AMC13 plugin:

  * AMC13 command classes have been moved into the ``swatch::amc13::cmds`` namespace, their code split up into more headers and cpp files (essentially one file per class), now located under ``swatch/amc13/cmds``

* Common MP7 plugin:

  * MP7 command classes have been moved into the ``swatch::mp7::cmds`` namespace, their code split up into more headers and cpp files (essentially one file per class), now located under ``swatch/mp7/cmds``


**System description file:**  No changes required.


**XML configuration files** (that store command parameters, monitoring settings and masks): 

* Common AMC13 plugin:

  * ``Cmd`` postfix has been removed from command IDs

* Common MP7 plugin:

  * The ID of the board reset command has been changed from ``resetBoard`` to ``reset``
