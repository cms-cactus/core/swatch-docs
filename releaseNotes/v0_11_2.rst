Version 0.11.2
==============

YUM repositories created on 06/02/2017 ; deployed at Point 5 on 07/02/2017

**Only one change with respect to 0.11.1:**

* Adapted SWATCH cell's DB queries to changes in schema that were deployed in production DB w/c 30th January 2017.


Migration guide: v0.11.1 to v0.11.2
-----------------------------------

**Note:** Subsystem software does NOT have to be recompiled after version 0.11.2 is installed.

**Installation:**

 * **N.B.** SWATCH v0.11.2 requires version 3.6 of the trigger supervisor libraries to be installed.
 * Centrally managed machines at Point 5 will be upgraded to version 0.11.2 on 07/02/2017
 * For centrally managed machines in building 904, please open a ticket at https://its.cern.ch/jira/projects/CMSONS/ to request the new version
 * For other machines ( *NOT* at Point 5 or in 904), the older packages must be first removed, following the commands in the installation section of the user's guide

**Source code:** No changes required


**System description file:**  No changes required.


**XML configuration files** (that store command parameters, monitoring settings and masks): No changes required.
