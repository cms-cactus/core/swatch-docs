AMC13
=====

Class: ``swatch::amc13::AMC13Manager``


Commands
--------


Reboot
^^^^^^

* Class: ``swatch::amc13::cmds::Reboot``
* Command id: ``reboot``
* Behaviour: Reboots AMC13 from onboard memory.
* Output: None
* Parameters: 

  =============   ============================   ==========================    ===============
  *id*            *description*                  *type*                        *Default value*
  =============   ============================   ==========================    ===============
  ``timeout``     Reboot timeout                 ``xdata::UnsignedInteger``    30
  =============   ============================   ==========================    ===============

Reset
^^^^^

* Class: ``swatch::amc13::cmds::Reset``
* Command id: ``reset``
* Behaviour: Resets AMC13 registers to baseline values.
* Output: None
* Parameters: None


Configure TTC
^^^^^^^^^^^^^

* Class: ``swatch::amc13::cmds::ConfigureTTC``
* Command id: ``configTCC``
* Behaviour: Configure TTC registers and enables clock and TTC command distribution to all AMCS.
* Output: None
* Parameters:

  =============   ============================   ==========================    ===============
  *id*            *description*                  *type*                        *Default value*
  =============   ============================   ==========================    ===============
  ``resyncCmd``   Resync command encoding        ``xdata::UnsignedInteger``    0x4
  ``ocrCmd``      Orbit reset command encoding   ``xdata::UnsignedInteger``    0x8
  ``localTTC``    Enable local ttc generator     ``xdata::Boolean``            false
  =============   ============================   ==========================    ===============


Configure readout and event builder
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Class: ``swatch::amc13::cmds::ConfigureDAQ``
* Command id: ``configDAQ``
* Behaviour: Configure DAQ registers and enables clock and TTC command distribution to all AMCS.
* Output: None
* Parameters:

  =============   ==========================================================   ==========================   ===============
  *id*            *description*                                                *type*                       *default value*
  =============   ==========================================================   ==========================   ===============
  ``slinkMask``   slink bitmask: bits 0, 1 and 2 represent the corresponding   ``xdata::UnsignedInteger``   0x0
                  AMC13 slink express (Usually set to 0x1)
  ``bcnOffset``   Event header bunch counter offset (Usually set to 3541)      ``xdata::UnsignedInteger``   3540
  =============   ==========================================================   ==========================   ===============


Start
^^^^^

"start"


Stop command
^^^^^^^^^^^^

"stop"
