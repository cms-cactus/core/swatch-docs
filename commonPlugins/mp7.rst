
MP7
===

The MP7 package provides a basic implementation of a SWATCH interface to MP7 boards. It comes in 2 flavours:

* ``swatch::mp7::MP7Processor`` full fledged implementation of an ``swatch::Processor`` for MP7 boards. Input and output ports are mapped to mp7 channels and standard MP7 commands are registered by default. Users must implement the algo interface.
* ``swatch::mp7::MP7AbstractProcessor`` abstract interface for boards using mp7 core firmware. MP7 Commands can be attached to classes derived from ``MP7AbstractProcessor``. Implementation of monitorables is left to users.
* ``swatch::mp7::MP7NullAlgoProcessor`` complete implementation of a ``null_algo``

General commands
----------------

Reset
^^^^^

* Class: ``swatch::mp7::cmds::Reset``
* Command id: ``resetBoard``
* Behaviour: Issues a global board reset, reconfigures clocks and ttc according to the selection
* Output: None
* Parameters:

  ===============   ===============================   =================   ===============
  *id*              *description*                     *type*              *default value*
  ===============   ===============================   =================   ===============
  ``clockSource``   external/internal                 ``xdata::String``   external
  ``clockConfig``   external/internal/(config name)   ``xdata::String``   external
  ``ttcConfig``     external/internal/(config name)   ``xdata::String``   external
  ===============   ===============================   =================   ===============


MMC
---

Scan SD card
^^^^^^^^^^^^
 
* Class: ``swatch::mp7::cmds::ScanSD``
* Command id: ``scanSD``
* Behaviour: Reads the list of available images fom the SD card.
* Output: None
* Parameters: None


Upload firmware image
^^^^^^^^^^^^^^^^^^^^^
 
* Class ``swatch::mp7::cmds::UploadFirmware``
* Command id: ``uploadFw``
* Behaviour: Uploads a firmware image ontu the MP7 SD card.
* Output: ``xdata::String`` containing the list of available images
* Parameters:

  =============   ========================================================   =================   ===============
  *id*            *description*                                              *type*              *default value*
  =============   ========================================================   =================   ===============
  ``localfile``   Path to the location of the new firmware image to upload   ``xdata::String``   ""
  ``sdfile``      Name of the firmware image on the SD card                  ``xdata::String``   ""
  =============   ========================================================   =================   ===============


Delete firmware image
^^^^^^^^^^^^^^^^^^^^^

* Class ``swatch::mp7::cmds::DeleteFirmware``
* Command id: ``deleteFw``
* Behaviour: Deletes a firmware image from the MP7 SD card.
* Output: None
* Parameters:

  ==========   =====================================   =================   ===============
  *id*         *description*                           *type*              *default value*
  ==========   =====================================   =================   ===============
  ``sdfile``   Name of the firmware image to delete    ``xdata::String``   ""        
  ==========   =====================================   =================   ===============

 
Reboot FPGA
^^^^^^^^^^^

* Class ``swatch::mp7::cmds::RebootFPGA``
* Command id: ``reboot``
* Behaviour: Reboots the MP7 FPGA to the selected firmware image.
* Output: None
* Parameters:

  ==========   =====================================   =================   ===============
  *id*         *description*                           *type*              *default value*
  ==========   =====================================   =================   ===============
  ``sdfile``   Name of the firmware image to boot to   ``xdata::String``   ""
  ==========   =====================================   =================   ===============


Hard reset
^^^^^^^^^^
* Class: ``swatch::mp7::cmds::HardReset``
* Command id: ``hardReset``
* Behaviour: Issues a hard reset to the FPGA..
* Output: None
* Parameters: None


MGTs
----

Configure input MGTs
^^^^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::ConfigureRxMGTs``
* Command id: ``cfgRxMGTs``
* Behaviour: Resets and configures MP7 input MGTs
* Output: None
* Parameters:

  ===================   =====================================================================   ==================   ===============
  *id*                  *description*                                                           *type*               *default value*
  ===================   =====================================================================   ==================   ===============
  ``ids``               Custom selection of inout MGTs Ids to configure                         ``xdata::String``    ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``                   ``xdata::String``    "apply"
  ``orbitTag``          Enable orbittag-based alignment                                         ``xdata::Boolean``   false
  ``polarity``          Link polarity: ``true`` standard polarity, ``false`` inverse polarity   ``xdata::Boolean``   ""
  ===================   =====================================================================   ==================   ===============


Configure output MGTs
^^^^^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::ConfigureTxMGTs``
* Command id: ``cfgTxMGTs``
* Behaviour: Resets and configures MP7 output MGTs
* Output: None
* Parameters:

  ============   =====================================================================   ==================   ===============
  *id*           *description*                                                           *type*               *default value*
  ============   =====================================================================   ==================   ===============
  ``ids``        Custom selection of output MGTs Ids to configure                        ``xdata::String``    ""
  ``orbitTag``   Enable orbittag-based alignment                                         ``xdata::Boolean``   false
  ``polarity``   Link polarity: ``true`` standard polarity, ``false`` inverse polarity   ``xdata::Boolean``   ""
  ============   =====================================================================   ==================   ===============


Align input MGTs
^^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::AlignRxsTo``
* Command id: ``alignMGTs``
* Behaviour: Aligns MP7 inputs to a specific orbit BX/Cycle
* Output: None
* Parameters:

  ===================   =====================================================   ==========================   ===============
  *id*                  *description*                                           *type*                       *default value*
  ===================   =====================================================   ==========================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``            ""          
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``            "apply"
  ``bx``, ``cycle``     Bx and cycle to aling inouts to                         ``xdata::UnsignedInteger``   0         
  ===================   =====================================================   ==========================   ===============


Auto align input MGTs
^^^^^^^^^^^^^^^^^^^^^
* Class: ``swatch::mp7::cmds::AutoAlign``
* Command id: ``autoAlignMGTs``
* Behaviour: Aligns MP7 inputs the minimum latency possible
* Output: None
* Parameters:

  ===================   =====================================================   =================   ===============
  *id*                  *description*                                           *type*              *default value* 
  ===================   =====================================================   =================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``   ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``   "apply"
  ===================   =====================================================   =================   ===============


Buffers
-------

Configure buffers
^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::ConfigureRxBuffers`` and ``swatch::mp7::cmds::ConfigureTxBuffers``
* Command id: ``latencyRxBuffers`` and ``latencyTxBuffers``
* Behaviour: Configure input or output buffers. Maks are taken into account for input buffers
* Output: None
* Parameters:

  ===========================   ===============================================   ==========================   ===============
  *id*                          *description*                                     *type*                       *default value*
  ===========================   ===============================================   ==========================   ===============
  ``ids``                       Custom selection of input MGTs Ids to configure   ``xdata::String``            ""
  ``masks`` (rx only)           Mask policy: can be =apply``, ``invert``,         ``xdata::String``            "apply"
                                ``ignore``
  ``mode``                      Buffer mode                                       ``xdata::String``            ""
  ``payload``                   Pattern to be loaded in the buffers if required   ``xdata::String``            ""
  ``startBx``, ``startCycle``   Beginning of the bunch crossing range (local      ``xdata::UnsignedInteger``   0,0
                                bunch counter)
  ``stopBx``, ``stopCycle``     End of the bunch crossing range (local bunch      ``xdata::UnsignedInteger``   NaN,NaN
                                counter) 
  ===========================   ===============================================   ==========================   ===============


Configure buffers in latency mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::LatencyRxBuffersCommand`` and ``swatch::mp7::cmds::LatencyTxBuffersCommand``
* Command id: ``cfgRxBuffers`` and ``cfgTxBuffers``
* Behaviour: Configure input or output buffers in latency mode. Maks are taken into account for input buffers
* Output: None
* Parameters:

  ===================   =====================================================   ==========================   ===============
  *id*                  *description*                                           *type*                       *default value*
  ===================   =====================================================   ==========================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``            ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``            "apply"  
  ``bankId``            MP7 bank id to assing to the buffers                    ``xdata::UnsignedInteger``   0
  ``depth``             Latency buffer depth (clock cycles)                     ``xdata::UnsignedInteger``   0
  ===================   =====================================================   ==========================   ===============


Configure buffers in latency mode (easy latency)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::EasyRxLatency`` and ``swatch::mp7::cmds::EasyTxLatency``
* Command id: ``easyRxLatency`` and ``easyTxLatency``
* Behaviour: Configure input or output buffers in latency mode from master and algo latency parameters. Maks are taken into account for input buffers
* Output: None
* Parameters:

  ===================   =====================================================   ==========================   ===============
  *id*                  *description*                                             *type*                     *default value*
  ===================   =====================================================   ==========================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``            ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``            "apply"
  ``bankId``            MP7 bank id to assing to the buffers                    ``xdata::UnsignedInteger``   0
  ``masterLatency``     Master latency (clock cycles)                           ``xdata::UnsignedInteger``   0
  ``internalLatency``   Internal latency (clock cycles, 32)                     ``xdata::UnsignedInteger``   0
  ``algoLatency``       Algorithm latency (clock cycles)                        ``xdata::UnsignedInteger``   0
  ``rxExtraFrames``     Additional bunch crossings in the rx readout window     ``xdata::UnsignedInteger``   0
                        ``N_bx^rx = 1+-rxExtraFrames``
  ``rxExtraFrames``     Additional bunch crossings in the tx readout window     ``xdata::UnsignedInteger``   0
                        ``N_bx^tx = 1+-txExtraFrames``
  ===================   =====================================================   ==========================   ===============


Save buffers
^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::SaveRxBuffersToFileCommand`` and ``swatch::mp7::cmds::SaveTxBuffersToFileCommand``
* Command id: ``saveRxBuffers`` and ``saveTxBuffers``
* Behaviour: Reads the content of input/output buffers and saves it to file 
* Output: None
* Parameters:

  ===================   =====================================================   =================   ===============
  *id*                  *description*                                           *type*              *default value*
  ===================   =====================================================   =================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``   ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``   "apply"
  ``filename``          Destination filename                                    ``xdata::String``   ""
  ===================   =====================================================   =================   ===============


Capture Buffers
^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::CaptureBuffers``
* Command id: ``capture``
* Behaviour: Issued a capture signal to MP7 buffers configured in capture mode. 
* Output: None
* Parameters: None


Formatters
----------

TDR formatter
^^^^^^^^^^^^^

* Class: ``TDRFormatterCommand``
* Command id: ``cfgFormatterTdr``
* Behaviour: Configure TDR formatter for input header stripping an re-insertion 
* Output: None
* Parameters:

  ===================   =====================================================   ==========================   ===============
  *id*                  *description*                                           *type*                       *default value*
  ===================   =====================================================   ==========================   ===============
  ``ids``               Custom selection of input MGTs Ids to configure         ``xdata::String``            ""
  ``masks`` (rx only)   Mask policy: can be ``apply``, ``invert``, ``ignore``   ``xdata::String``            "apply"
  ``insert``            Enables header stripping                                ``xdata::Boolean``           true
  ``strip``             Enables header insertion                                ``xdata::Boolean``           true 
  ===================   =====================================================   ==========================   ===============


Demux formatter
^^^^^^^^^^^^^^^

* Class: ``DemuxFormatterCommand``
* Command id: Not yet registered
* Behaviour: Configure Demux formatter for input header stripping an re-insertion and, optionally, datavalid override.
* Output: None
* Parameters:

  ===========================   ===============================================   ==========================   ===============
  *id*                          *description*                                     *type*                       *default value*
  ===========================   ===============================================   ==========================   ===============
  ``ids``                       Custom selection of input MGTs Ids to configure   ``xdata::String``            ""
  ``masks`` (rx only)           Mask policy: can be ``apply``, ``invert``,        ``xdata::String``            "apply"
                                ``ignore``
  ``insert``                    Enables header stripping                          ``xdata::Boolean``           true
  ``strip``                     Enables header insertion                          ``xdata::Boolean``           true
  ``startBx``, ``startCycle``   Beginning of the bunch crossing range (local      ``xdata::UnsignedInteger``   0,0
                                bunch counter)
  ``stopBx``, ``stopCycle``     End of the bunch crossing range (local bunch      ``xdata::UnsignedInteger``   NaN,NaN
                                counter)
  ===========================   ===============================================   ==========================   ===============


Readout
-------

Configure readout
^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::SetupReadout``
* Command id: ``roSetup``
* Behaviour: Configure MP7 readout block
* Output: None
* Parameters:

  =============   ===========================================   ==========================   ===============
  *id*            *description*                                 *type*                       *default value*
  =============   ===========================================   ==========================   ===============
  ``internal``    Sets up internal readout mode for debugging   ``xdata::Boolean``           false
  ``bxOffset``    Bunch crossing counter offset                 ``xdata::UnsignedInteger``   1
  ``drain``       Drain rate, disabled by default               ``xdata::UnsignedInteger``   NaN
  ``bufferHWM``   Readout buffer high watermark                 ``xdata::UnsignedInteger``   32
  ``bufferLWM``   Readout buffer low watermark                  ``xdata::UnsignedInteger``   16
  ``fake``        Fake event size, disabled by default          ``xdata::UnsignedInteger``   NaN
  =============   ===========================================   ==========================   ===============


Load readout menu
^^^^^^^^^^^^^^^^^

* Class: ``swatch::mp7::cmds::LoadReadoutMenu``
* Command id: ``roLoadMenu``
* Behaviour: Loads readout menu onto the board 
* Output: None
* Parameters:

  * Bank specific parameters for mode ``[bid]``

    ========================   ========================================================   ==========================   ===============
    *id*                       *description*                                              *type*                       *default value*
    ========================   ========================================================   ==========================   ===============
    ``bank[bid]:wordsPerBx``   Number of 64b words to be capture per bx by bank ``bid``   ``xdata::UnsignedInteger``   false
    ========================   ========================================================   ==========================   ===============

  * Trigger mode parameters for mode ``[mid]``

    ============================   ==================================================   ==========================   ===============
    *id*                           *description*                                        *type*                       *default value*
    ============================   ==================================================   ==========================   ===============
    ``mode[mid]:eventSize``        Expected event size in 64 words for mode ``[mid]``   ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:eventToTrigger``   Events which activate trigger mode ``[mid]``         ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:eventType``        Event type. Written in the header                    ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:tokenDelay``       Readout token delay                                  ``xdata::UnsignedInteger``   NaN
    ============================   ==================================================   ==========================   ===============

  * Capture mode for trigger mode ``[mid]`` and capture mode ``[cid]``

    ========================================   ========================================================   ==========================   ===============
    *id*                                       *description*                                              *type*                       *default value*
    ========================================   ========================================================   ==========================   ===============
    ``mode[mid]:capture[cid]:enable``          Enable capture mode ``[cid]`` for trigger mode ``[mid]``   ``xdata::Boolean``           false
    ``mode[mid]:capture[cid]:id``              Capture id. Written in the capture block header            ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:capture[cid]:bankId``          Bank id captured by this capture mode                      ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:capture[cid]:length``          Number of 64b words per channel sent to the readout        ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:capture[cid]:delay``           Capture delay in BXs                                       ``xdata::UnsignedInteger``   NaN
    ``mode[mid]:capture[cid]:readoutLength``   Block readout length in clock cycles                       ``xdata::UnsignedInteger``   NaN
    ========================================   ========================================================   ==========================   ===============


