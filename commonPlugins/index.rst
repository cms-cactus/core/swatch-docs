.. testdrive documentation master file, created by
   sphinx-quickstart on Sat Jan 23 17:27:43 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Common plugins
==============

Welcome to the documentation for the common SWATCH plugins!

Contents:

.. toctree::
   :maxdepth: 2

   amc13
   mp7



